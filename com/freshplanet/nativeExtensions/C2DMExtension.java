package com.freshplanet.nativeExtensions;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class C2DMExtension implements FREExtension {
    private static String PREFERENCES_NAME;
    private static String TAG;
    public static FREContext context;
    public static boolean isInForeground;
    public static int lastNotificationId;
    public static int m_nPocetSprav;
    public static HashMap<String, Integer> mapNotificationId;
    public static HashMap<String, ArrayList<String>> mapNotificationTexts;
    public static HashMap<String, Integer> mapNumNotifications;

    static {
        TAG = "c2dmExtension";
        PREFERENCES_NAME = "EdupagePreferences";
        isInForeground = false;
        m_nPocetSprav = 0;
        mapNotificationId = new HashMap();
        mapNumNotifications = new HashMap();
        mapNotificationTexts = new HashMap();
        lastNotificationId = 1;
    }

    public FREContext createContext(String extId) {
        Log.d(TAG, "C2DMExtension.createContext extId: " + extId);
        context = new C2DMExtensionContext();
        return context;
    }

    public void dispose() {
        Log.d(TAG, "C2DMExtension.dispose");
        context = null;
    }

    public void initialize() {
        Log.d(TAG, "C2DMExtension.initialize");
    }

    public static void loadData(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCES_NAME, 0);
        try {
            String key;
            m_nPocetSprav = settings.getInt("m_nPocetSprav", 0);
            JSONObject mapNotificationIdData = new JSONObject(settings.getString("mapNotificationId", "{}"));
            mapNotificationId = new HashMap();
            Iterator<String> iterator = mapNotificationIdData.keys();
            while (iterator.hasNext()) {
                key = (String) iterator.next();
                mapNotificationId.put(key, Integer.valueOf(mapNotificationIdData.getInt(key)));
            }
            JSONObject mapNumNotificationsData = new JSONObject(settings.getString("mapNumNotifications", "{}"));
            mapNumNotifications = new HashMap();
            iterator = mapNumNotificationsData.keys();
            while (iterator.hasNext()) {
                key = (String) iterator.next();
                mapNumNotifications.put(key, Integer.valueOf(mapNumNotificationsData.getInt(key)));
            }
            JSONObject mapNotificationTextsData = new JSONObject(settings.getString("mapNotificationTexts", "{}"));
            mapNotificationTexts = new HashMap();
            iterator = mapNotificationTextsData.keys();
            while (iterator.hasNext()) {
                key = (String) iterator.next();
                JSONArray textsData = mapNotificationTextsData.getJSONArray(key);
                ArrayList<String> texts = new ArrayList();
                for (int i = 0; i < textsData.length(); i++) {
                    texts.add(textsData.getString(i));
                }
                mapNotificationTexts.put(key, texts);
            }
            lastNotificationId = settings.getInt("lastNotificationId", 1);
        } catch (JSONException e) {
        }
    }

    public static void saveData(Context context) {
        Editor editor = context.getSharedPreferences(PREFERENCES_NAME, 0).edit();
        try {
            editor.putInt("m_nPocetSprav", m_nPocetSprav);
            JSONObject mapNotificationIdData = new JSONObject();
            for (String key : mapNotificationId.keySet()) {
                mapNotificationIdData.put(key, mapNotificationId.get(key));
            }
            editor.putString("mapNotificationId", mapNotificationIdData.toString());
            JSONObject mapNumNotificationsData = new JSONObject();
            for (String key2 : mapNumNotifications.keySet()) {
                mapNumNotificationsData.put(key2, mapNumNotifications.get(key2));
            }
            editor.putString("mapNumNotifications", mapNumNotificationsData.toString());
            JSONObject mapNotificationTextsData = new JSONObject();
            for (String key22 : mapNotificationTexts.keySet()) {
                ArrayList<String> texts = (ArrayList) mapNotificationTexts.get(key22);
                JSONArray textsData = new JSONArray();
                for (int i = 0; i < texts.size(); i++) {
                    textsData.put(texts.get(i));
                }
                mapNotificationTextsData.put(key22, textsData);
            }
            editor.putString("mapNotificationTexts", mapNotificationTextsData.toString());
            editor.putInt("lastNotificationId", lastNotificationId);
        } catch (JSONException e) {
        }
        editor.commit();
    }
}
