package org.edupage.external;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import java.util.HashMap;
import java.util.Map;

public class ExternalContext extends FREContext {
    public Map<String, FREFunction> getFunctions() {
        Map<String, FREFunction> functions = new HashMap();
        functions.put("initialize", new InitializeFunction());
        functions.put("open", new OpenFunction());
        return functions;
    }

    public void dispose() {
    }
}
