package org.edupage.external;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class ExternalExtension implements FREExtension {
    public static ExternalContext context;

    public void initialize() {
    }

    public FREContext createContext(String contextType) {
        context = new ExternalContext();
        return context;
    }

    public void dispose() {
    }
}
