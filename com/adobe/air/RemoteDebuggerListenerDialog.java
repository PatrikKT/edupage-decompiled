package com.adobe.air;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.appcompat.C0007R;
import android.view.KeyEvent;
import java.net.InetAddress;
import java.net.Socket;

public class RemoteDebuggerListenerDialog extends Activity {
    private final String LOG_TAG;
    private int count;
    private int debuggerPort;
    private Activity mActivity;
    private Runnable mCheckAgain;
    private Handler mHandler;
    private BroadcastReceiver mReceiver;
    private AlertDialog mWaitDialog;

    /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.1 */
    class C00591 extends BroadcastReceiver {
        C00591() {
        }

        public void onReceive(Context context, Intent intent) {
            if (!isInitialStickyBroadcast()) {
                Bundle extras = RemoteDebuggerListenerDialog.this.getIntent().getExtras();
                if ((extras != null ? extras.getInt("debuggerPort") : 7936) == RemoteDebuggerListenerDialog.this.debuggerPort) {
                    RemoteDebuggerListenerDialog.this.dismissDialog();
                }
            }
        }
    }

    /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.2 */
    class C00602 implements OnClickListener {
        C00602() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            RemoteDebuggerListenerDialog.this.mHandler.removeCallbacks(RemoteDebuggerListenerDialog.this.mCheckAgain);
            RemoteDebuggerListenerDialog.this.closeListeningDebuggerSocket();
            RemoteDebuggerListenerDialog.this.unregisterReceiver(RemoteDebuggerListenerDialog.this.mReceiver);
            RemoteDebuggerListenerDialog.this.mReceiver = null;
            dialogInterface.cancel();
            RemoteDebuggerListenerDialog.this.finish();
        }
    }

    /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.3 */
    class C00613 implements OnKeyListener {
        C00613() {
        }

        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i == 4) {
                RemoteDebuggerListenerDialog.this.mHandler.removeCallbacks(RemoteDebuggerListenerDialog.this.mCheckAgain);
                RemoteDebuggerListenerDialog.this.closeListeningDebuggerSocket();
                RemoteDebuggerListenerDialog.this.unregisterReceiver(RemoteDebuggerListenerDialog.this.mReceiver);
                RemoteDebuggerListenerDialog.this.mReceiver = null;
                dialogInterface.cancel();
                RemoteDebuggerListenerDialog.this.finish();
            }
            return false;
        }
    }

    /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.4 */
    class C00644 implements Runnable {
        final /* synthetic */ String val$dialogMessage;
        final /* synthetic */ String val$timeOutMessage;

        /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.4.1 */
        class C00621 implements OnClickListener {
            C00621() {
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                RemoteDebuggerListenerDialog.this.closeListeningDebuggerSocket();
                dialogInterface.cancel();
                RemoteDebuggerListenerDialog.this.finish();
            }
        }

        /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.4.2 */
        class C00632 implements OnKeyListener {
            final /* synthetic */ OnClickListener val$dialogButtonHandler;

            C00632(OnClickListener onClickListener) {
                this.val$dialogButtonHandler = onClickListener;
            }

            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == 4) {
                    this.val$dialogButtonHandler.onClick(dialogInterface, -1);
                }
                return false;
            }
        }

        C00644(String str, String str2) {
            this.val$dialogMessage = str;
            this.val$timeOutMessage = str2;
        }

        public void run() {
            if (RemoteDebuggerListenerDialog.this.count < 60) {
                CharSequence format = String.format(this.val$dialogMessage, new Object[]{Integer.valueOf(60 - RemoteDebuggerListenerDialog.this.count)});
                RemoteDebuggerListenerDialog.this.count = RemoteDebuggerListenerDialog.this.count + 1;
                RemoteDebuggerListenerDialog.this.mWaitDialog.setMessage(format);
                RemoteDebuggerListenerDialog.this.mHandler.postDelayed(this, 1000);
                return;
            }
            RemoteDebuggerListenerDialog.this.mHandler.removeCallbacks(this);
            RemoteDebuggerListenerDialog.this.mWaitDialog.cancel();
            if (RemoteDebuggerListenerDialog.this.mReceiver != null) {
                RemoteDebuggerListenerDialog.this.unregisterReceiver(RemoteDebuggerListenerDialog.this.mReceiver);
                RemoteDebuggerListenerDialog.this.mReceiver = null;
            }
            OnClickListener c00621 = new C00621();
            RemoteDebuggerListenerDialog.this.mWaitDialog = RemoteDebuggerListenerDialog.this.createDialog(AndroidConstants.ADOBE_AIR, this.val$timeOutMessage, RemoteDebuggerListenerDialog.this.getString(C0007R.string.button_continue), c00621, new C00632(c00621));
            RemoteDebuggerListenerDialog.this.mWaitDialog.show();
        }
    }

    /* renamed from: com.adobe.air.RemoteDebuggerListenerDialog.5 */
    class C00655 extends AsyncTask<Integer, Integer, Integer> {
        C00655() {
        }

        protected Integer doInBackground(Integer... numArr) {
            try {
                new Socket(InetAddress.getLocalHost(), numArr[0].intValue()).close();
            } catch (Exception e) {
            }
            return Integer.valueOf(0);
        }
    }

    private enum DialogState {
        StateRuntimeNotReady,
        StateRuntimeWaitingForDebugger,
        StateRuntimeTimedOut
    }

    public RemoteDebuggerListenerDialog() {
        this.debuggerPort = -1;
        this.count = 0;
        this.mHandler = new Handler();
        this.mWaitDialog = null;
        this.mCheckAgain = null;
        this.mActivity = null;
        this.LOG_TAG = getClass().getName();
    }

    public void onCreate(Bundle bundle) {
        String string = getString(C0007R.string.IDA_APP_WAITING_DEBUGGER_WARNING);
        String string2 = getString(C0007R.string.IDA_APP_DEBUGGER_TIMEOUT_INFO);
        this.mActivity = this;
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        this.debuggerPort = extras != null ? extras.getInt("debuggerPort") : 7936;
        this.mWaitDialog = new Builder(this).create();
        CharSequence format = String.format(string, new Object[]{Integer.valueOf(60)});
        this.mReceiver = new C00591();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.MAIN");
        intentFilter.addCategory("RemoteDebuggerListenerDialogClose");
        registerReceiver(this.mReceiver, intentFilter);
        this.mWaitDialog = createDialog(getString(C0007R.string.IDA_APP_WAITING_DEBUGGER_TITLE), format, getString(C0007R.string.button_cancel), new C00602(), new C00613());
        this.count = 0;
        this.mCheckAgain = new C00644(string, string2);
        this.mHandler.postDelayed(this.mCheckAgain, 1000);
        this.mWaitDialog.show();
    }

    private AlertDialog createDialog(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, OnClickListener onClickListener, OnKeyListener onKeyListener) {
        AlertDialog create = new Builder(this.mActivity).create();
        create.setTitle(charSequence);
        create.setMessage(charSequence2);
        create.setButton(-1, charSequence3, onClickListener);
        create.setOnKeyListener(onKeyListener);
        create.setCancelable(true);
        return create;
    }

    private void closeListeningDebuggerSocket() {
        new C00655().execute(new Integer[]{Integer.valueOf(this.debuggerPort)});
    }

    private void dismissDialog() {
        if (this.mWaitDialog != null) {
            this.mWaitDialog.cancel();
        }
        if (this.mReceiver != null) {
            unregisterReceiver(this.mReceiver);
        }
        this.mReceiver = null;
        this.mHandler.removeCallbacks(this.mCheckAgain);
        this.mActivity.finish();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            closeListeningDebuggerSocket();
            dismissDialog();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onStop() {
        closeListeningDebuggerSocket();
        dismissDialog();
        super.onStop();
    }
}
