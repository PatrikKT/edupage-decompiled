package com.adobe.air.wand.view;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import com.adobe.air.AndroidActivityWrapper;
import com.adobe.air.AndroidActivityWrapper.PlaneID;
import com.adobe.air.TouchEventData;

public class CompanionView extends View {
    private static final String LOG_TAG = "CompanionView";
    static final int POST_TOUCH_MESSAGE_AFTER_DELAY = 0;
    public static final int kTouchActionBegin = 2;
    public static final int kTouchActionEnd = 4;
    public static final int kTouchActionMove = 1;
    public static final int kTouchMetaStateIsEraser = 67108864;
    public static final int kTouchMetaStateIsPen = 33554432;
    public static final int kTouchMetaStateMask = 234881024;
    public static final int kTouchMetaStateSideButton1 = 134217728;
    public final int kMultitouchGesture;
    public final int kMultitouchNone;
    public final int kMultitouchRaw;
    private int mBoundHeight;
    private int mBoundWidth;
    private int mCurrentOrientation;
    private GestureDetector mGestureDetector;
    private GestureListener mGestureListener;
    private boolean mIsFullScreen;
    private int mMultitouchMode;
    private ScaleGestureDetector mScaleGestureDetector;
    private int mSkipHeightFromTop;
    private TouchSensor mTouchSensor;
    private int mVisibleBoundHeight;
    private int mVisibleBoundWidth;

    public CompanionView(Context context) {
        super(context);
        this.kMultitouchNone = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.kMultitouchRaw = kTouchActionMove;
        this.kMultitouchGesture = kTouchActionBegin;
        this.mSkipHeightFromTop = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mBoundHeight = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mBoundWidth = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mVisibleBoundWidth = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mVisibleBoundHeight = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mMultitouchMode = kTouchActionBegin;
        this.mCurrentOrientation = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mIsFullScreen = false;
        this.mTouchSensor = null;
        initView(context);
    }

    public CompanionView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.kMultitouchNone = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.kMultitouchRaw = kTouchActionMove;
        this.kMultitouchGesture = kTouchActionBegin;
        this.mSkipHeightFromTop = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mBoundHeight = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mBoundWidth = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mVisibleBoundWidth = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mVisibleBoundHeight = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mMultitouchMode = kTouchActionBegin;
        this.mCurrentOrientation = POST_TOUCH_MESSAGE_AFTER_DELAY;
        this.mIsFullScreen = false;
        this.mTouchSensor = null;
        initView(context);
    }

    private void initView(Context context) {
        this.mTouchSensor = new TouchSensor();
        this.mGestureListener = new GestureListener(context, this);
        this.mGestureDetector = new GestureDetector(context, this.mGestureListener, null, false);
        this.mScaleGestureDetector = new ScaleGestureDetector(context, this.mGestureListener);
        setWillNotDraw(false);
        setClickable(true);
        setEnabled(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public View returnThis() {
        return this;
    }

    public void onWindowFocusChanged(boolean z) {
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return false;
    }

    protected void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i;
        boolean z;
        int action = motionEvent.getAction() & 255;
        if (this.mGestureListener != null) {
            if (action == 5 || action == 0) {
                for (i = POST_TOUCH_MESSAGE_AFTER_DELAY; i < motionEvent.getPointerCount(); i += kTouchActionMove) {
                    this.mGestureListener.setDownTouchPoint(motionEvent.getX(i), motionEvent.getY(i), motionEvent.getPointerId(i));
                }
            }
            if (action == 0) {
                this.mGestureListener.mayStartNewTransformGesture();
            }
            if (action == 5) {
                this.mGestureListener.setCouldBeTwoFingerTap(kTouchActionMove);
                this.mGestureListener.setPrimaryPointOfTwoFingerTap(motionEvent.getX(POST_TOUCH_MESSAGE_AFTER_DELAY), motionEvent.getY(POST_TOUCH_MESSAGE_AFTER_DELAY), motionEvent.getPointerId(POST_TOUCH_MESSAGE_AFTER_DELAY));
                this.mGestureListener.setSecondaryPointOfTwoFingerTap(motionEvent.getX(kTouchActionMove), motionEvent.getY(kTouchActionMove), motionEvent.getPointerId(kTouchActionMove));
            } else if (action == 6 && this.mGestureListener.getCouldBeTwoFingerTap() == kTouchActionMove) {
                this.mGestureListener.setCouldBeTwoFingerTap(kTouchActionBegin);
            } else if (action == kTouchActionMove && this.mGestureListener.getCouldBeTwoFingerTap() == kTouchActionBegin) {
                this.mGestureListener.setCouldBeTwoFingerTap(3);
            } else if (action != kTouchActionBegin) {
                this.mGestureListener.setCouldBeTwoFingerTap(POST_TOUCH_MESSAGE_AFTER_DELAY);
            }
        }
        int pointerCount = motionEvent.getPointerCount();
        Object obj = null;
        int i2 = POST_TOUCH_MESSAGE_AFTER_DELAY;
        while (i2 < pointerCount) {
            int toolType;
            int i3;
            Object obj2;
            Object obj3;
            float size;
            boolean z2;
            int historySize;
            float[] fArr;
            float pressure;
            float x = motionEvent.getX(i2);
            float y = ((float) this.mSkipHeightFromTop) + motionEvent.getY(i2);
            int action2 = motionEvent.getAction();
            int pointerId = motionEvent.getPointerId(i2);
            i = motionEvent.getMetaState();
            if (VERSION.SDK_INT >= 14) {
                i &= -234881025;
                toolType = motionEvent.getToolType(i2);
                if (toolType == kTouchActionEnd) {
                    i |= kTouchMetaStateIsEraser;
                } else if (toolType == kTouchActionBegin) {
                    i |= kTouchMetaStateIsPen;
                }
                if ((motionEvent.getButtonState() & kTouchActionBegin) != 0) {
                    i3 = i | kTouchMetaStateSideButton1;
                    if (obj == null) {
                        i = 3;
                        obj2 = obj;
                        action = kTouchActionEnd;
                    } else {
                        if (motionEvent.getPointerCount() != kTouchActionMove) {
                            if (pointerId != motionEvent.getPointerId((65280 & action2) >> 8)) {
                                i = action2;
                                obj2 = obj;
                                action = kTouchActionMove;
                            }
                        }
                        action2 &= 255;
                        switch (action2) {
                            case POST_TOUCH_MESSAGE_AFTER_DELAY /*0*/:
                            case AndroidActivityWrapper.WEBVIEW_UPLOAD_FILE_CHOOSER_CODE /*5*/:
                                obj2 = obj;
                                action = kTouchActionBegin;
                                i = action2;
                                break;
                            case kTouchActionMove /*1*/:
                            case PlaneID.PLANE_STAGE3D /*6*/:
                                obj3 = obj;
                                break;
                            case GestureListener.kGestureTap /*3*/:
                                obj3 = kTouchActionMove;
                                break;
                            default:
                                obj2 = obj;
                                action = kTouchActionMove;
                                i = action2;
                                break;
                        }
                        action = kTouchActionEnd;
                        if (this.mGestureListener == null) {
                            this.mGestureListener.endTwoFingerGesture();
                            this.mGestureListener.setCheckForSwipe(true);
                            obj2 = obj3;
                            i = action2;
                        } else {
                            obj2 = obj3;
                            i = action2;
                        }
                    }
                    if (IsTouchEventHandlingAllowed(action, x, y)) {
                        size = motionEvent.getSize(i2);
                        z2 = pointerId != 0;
                        historySize = motionEvent.getHistorySize();
                        fArr = new float[((historySize + kTouchActionMove) * 3)];
                        toolType = POST_TOUCH_MESSAGE_AFTER_DELAY;
                        for (action2 = POST_TOUCH_MESSAGE_AFTER_DELAY; action2 < historySize; action2 += kTouchActionMove) {
                            int i4 = toolType + kTouchActionMove;
                            fArr[toolType] = motionEvent.getHistoricalX(i2, action2);
                            int i5 = i4 + kTouchActionMove;
                            fArr[i4] = motionEvent.getHistoricalY(i2, action2);
                            toolType = i5 + kTouchActionMove;
                            fArr[i5] = motionEvent.getHistoricalPressure(i2, action2);
                        }
                        pressure = motionEvent.getPressure(i2);
                        fArr[toolType] = x;
                        fArr[toolType + kTouchActionMove] = y;
                        fArr[toolType + kTouchActionBegin] = pressure;
                        historySize = i3 & -2;
                        if (action != kTouchActionMove) {
                            if (i == 3) {
                                historySize |= kTouchActionMove;
                            }
                            this.mTouchSensor.dispatchEvent(new TouchEventData(action, x, y, pressure, pointerId, size, size, z2, null, historySize));
                        }
                        switch (action) {
                            case kTouchActionMove /*1*/:
                            case kTouchActionBegin /*2*/:
                                action = kTouchActionMove;
                                break;
                            default:
                                action = POST_TOUCH_MESSAGE_AFTER_DELAY;
                                break;
                        }
                        if (action != 0) {
                            this.mTouchSensor.dispatchEvent(new TouchEventData(action, x, y, pressure, pointerId, size, size, z2, fArr, historySize));
                        }
                    }
                    i2 += kTouchActionMove;
                    obj = obj2;
                }
            }
            i3 = i;
            if (obj == null) {
                if (motionEvent.getPointerCount() != kTouchActionMove) {
                    if (pointerId != motionEvent.getPointerId((65280 & action2) >> 8)) {
                        i = action2;
                        obj2 = obj;
                        action = kTouchActionMove;
                    }
                }
                action2 &= 255;
                switch (action2) {
                    case POST_TOUCH_MESSAGE_AFTER_DELAY /*0*/:
                    case AndroidActivityWrapper.WEBVIEW_UPLOAD_FILE_CHOOSER_CODE /*5*/:
                        obj2 = obj;
                        action = kTouchActionBegin;
                        i = action2;
                        break;
                    case kTouchActionMove /*1*/:
                    case PlaneID.PLANE_STAGE3D /*6*/:
                        obj3 = obj;
                        break;
                    case GestureListener.kGestureTap /*3*/:
                        obj3 = kTouchActionMove;
                        break;
                    default:
                        obj2 = obj;
                        action = kTouchActionMove;
                        i = action2;
                        break;
                }
                action = kTouchActionEnd;
                if (this.mGestureListener == null) {
                    obj2 = obj3;
                    i = action2;
                } else {
                    this.mGestureListener.endTwoFingerGesture();
                    this.mGestureListener.setCheckForSwipe(true);
                    obj2 = obj3;
                    i = action2;
                }
            } else {
                i = 3;
                obj2 = obj;
                action = kTouchActionEnd;
            }
            if (IsTouchEventHandlingAllowed(action, x, y)) {
                size = motionEvent.getSize(i2);
                if (pointerId != 0) {
                }
                historySize = motionEvent.getHistorySize();
                fArr = new float[((historySize + kTouchActionMove) * 3)];
                toolType = POST_TOUCH_MESSAGE_AFTER_DELAY;
                for (action2 = POST_TOUCH_MESSAGE_AFTER_DELAY; action2 < historySize; action2 += kTouchActionMove) {
                    int i42 = toolType + kTouchActionMove;
                    fArr[toolType] = motionEvent.getHistoricalX(i2, action2);
                    int i52 = i42 + kTouchActionMove;
                    fArr[i42] = motionEvent.getHistoricalY(i2, action2);
                    toolType = i52 + kTouchActionMove;
                    fArr[i52] = motionEvent.getHistoricalPressure(i2, action2);
                }
                pressure = motionEvent.getPressure(i2);
                fArr[toolType] = x;
                fArr[toolType + kTouchActionMove] = y;
                fArr[toolType + kTouchActionBegin] = pressure;
                historySize = i3 & -2;
                if (action != kTouchActionMove) {
                    if (i == 3) {
                        historySize |= kTouchActionMove;
                    }
                    this.mTouchSensor.dispatchEvent(new TouchEventData(action, x, y, pressure, pointerId, size, size, z2, null, historySize));
                }
                switch (action) {
                    case kTouchActionMove /*1*/:
                    case kTouchActionBegin /*2*/:
                        action = kTouchActionMove;
                        break;
                    default:
                        action = POST_TOUCH_MESSAGE_AFTER_DELAY;
                        break;
                }
                if (action != 0) {
                    this.mTouchSensor.dispatchEvent(new TouchEventData(action, x, y, pressure, pointerId, size, size, z2, fArr, historySize));
                }
            }
            i2 += kTouchActionMove;
            obj = obj2;
        }
        if (this.mScaleGestureDetector != null) {
            try {
                z = this.mScaleGestureDetector.onTouchEvent(motionEvent);
            } catch (Exception e) {
                z = true;
            }
        } else {
            z = true;
        }
        if (this.mGestureDetector != null) {
            return z && this.mGestureDetector.onTouchEvent(motionEvent);
        } else {
            return z;
        }
    }

    public void onGestureListener(int i, int i2, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        this.mTouchSensor.dispatchEvent(new GestureEventData(i, i2, z, f, f2, f3, f4, f5, f6, f7));
    }

    public TouchSensor getTouchSensor() {
        return this.mTouchSensor;
    }

    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
    }

    public void setMultitouchMode(int i) {
        this.mMultitouchMode = i;
    }

    public int getMultitouchMode() {
        return this.mMultitouchMode;
    }

    public boolean getIsFullScreen() {
        return this.mIsFullScreen;
    }

    public int getBoundWidth() {
        return this.mBoundWidth;
    }

    public int getBoundHeight() {
        return this.mBoundHeight;
    }

    public int getVisibleBoundWidth() {
        return this.mVisibleBoundWidth;
    }

    public int getVisibleBoundHeight() {
        return this.mVisibleBoundHeight;
    }

    public boolean IsLandScape() {
        return this.mCurrentOrientation == kTouchActionBegin;
    }

    private boolean IsTouchEventHandlingAllowed(int i, float f, float f2) {
        return true;
    }

    public boolean IsTouchUpHandlingAllowed() {
        return true;
    }
}
