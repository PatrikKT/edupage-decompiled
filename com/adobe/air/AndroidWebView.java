package com.adobe.air;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.adobe.air.AndroidActivityWrapper.ActivityState;
import com.adobe.air.wand.view.CompanionView;
import java.io.UnsupportedEncodingException;

public class AndroidWebView implements StateChangeCallback {
    private static final int ERROR_OTHER = 0;
    private static final int ERROR_PROTOCOL_UNSUPPORTED = 3229;
    private static final int FOCUS_DOWN = 3;
    private static final int FOCUS_NONE = 1;
    private static final int FOCUS_UP = 2;
    private static final String LOG_TAG = "AndroidWebView";
    private AIRWindowSurfaceView mAIRSurface;
    private Rect mBounds;
    private Context mContext;
    private WebViewCustomView mCustomViewHolder;
    private Rect mGlobalBounds;
    private long mInternalReference;
    private RelativeLayout mLayout;
    private String mUrl;
    private AndroidWebViewImpl mView;

    /* renamed from: com.adobe.air.AndroidWebView.1 */
    class C00361 extends WebChromeClient {
        private ValueCallback<Uri> mUploadMessage;

        /* renamed from: com.adobe.air.AndroidWebView.1.1 */
        class C00351 implements ActivityResultCallback {
            final /* synthetic */ AndroidActivityWrapper val$wrapper;

            C00351(AndroidActivityWrapper androidActivityWrapper) {
                this.val$wrapper = androidActivityWrapper;
            }

            public void onActivityResult(int i, int i2, Intent intent) {
                if (i == 5) {
                    if (C00361.this.mUploadMessage != null) {
                        Object data = (intent == null || i2 != -1) ? null : intent.getData();
                        C00361.this.mUploadMessage.onReceiveValue(data);
                        C00361.this.mUploadMessage = null;
                    }
                    this.val$wrapper.removeActivityResultListener(this);
                }
            }
        }

        C00361() {
        }

        public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
            if (AndroidWebView.this.mCustomViewHolder == null) {
                AndroidWebView.this.mCustomViewHolder = new WebViewCustomView();
            }
            AndroidWebView.this.mCustomViewHolder.onShowCustomView(view, customViewCallback);
        }

        public void onHideCustomView() {
            if (AndroidWebView.this.mCustomViewHolder != null) {
                AndroidWebView.this.mCustomViewHolder.onHideCustomView();
            }
        }

        public void openFileChooser(ValueCallback<Uri> valueCallback) {
            if (this.mUploadMessage == null) {
                this.mUploadMessage = valueCallback;
                AndroidActivityWrapper GetAndroidActivityWrapper = AndroidActivityWrapper.GetAndroidActivityWrapper();
                GetAndroidActivityWrapper.addActivityResultListener(new C00351(GetAndroidActivityWrapper));
                Intent intent = new Intent("android.intent.action.GET_CONTENT");
                intent.addCategory("android.intent.category.OPENABLE");
                intent.setType("*/*");
                GetAndroidActivityWrapper.getActivity().startActivityForResult(Intent.createChooser(intent, ""), 5);
            }
        }
    }

    /* renamed from: com.adobe.air.AndroidWebView.2 */
    class C00382 extends WebViewClient {
        private String mLastPageStartedUrl;
        private String mNoCompleteForUrl;
        private String mUrl;
        final /* synthetic */ AndroidWebView val$webView;

        /* renamed from: com.adobe.air.AndroidWebView.2.1 */
        class C00371 extends Thread {
            final /* synthetic */ SslError val$error;
            final /* synthetic */ SslErrorHandler val$handler;
            final /* synthetic */ String val$url;

            C00371(SslError sslError, String str, SslErrorHandler sslErrorHandler) {
                this.val$error = sslError;
                this.val$url = str;
                this.val$handler = sslErrorHandler;
            }

            public void run() {
                Object obj = null;
                if (this.val$error.getCertificate() != null) {
                    SSLSecurityDialog sSLSecurityDialog = new SSLSecurityDialog();
                    sSLSecurityDialog.show(this.val$url, this.val$error.getCertificate());
                    if (sSLSecurityDialog.getUserAction().equals("session")) {
                        obj = AndroidWebView.FOCUS_NONE;
                    }
                }
                if (obj != null) {
                    this.val$handler.proceed();
                } else {
                    this.val$handler.cancel();
                }
            }
        }

        C00382(AndroidWebView androidWebView) {
            this.val$webView = androidWebView;
            this.mUrl = null;
            this.mLastPageStartedUrl = null;
            this.mNoCompleteForUrl = null;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            this.mUrl = str;
            this.mLastPageStartedUrl = str;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            boolean onLocationChanging = this.val$webView.onLocationChanging(str);
            if (onLocationChanging) {
                this.mNoCompleteForUrl = str;
            }
            return onLocationChanging;
        }

        public void onPageFinished(WebView webView, String str) {
            if (str == null || this.mNoCompleteForUrl == null || !str.equals(this.mNoCompleteForUrl)) {
                this.mUrl = str;
                this.val$webView.onLoadComplete(str);
                return;
            }
            this.mNoCompleteForUrl = null;
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            if (this.mUrl != null && sslError.getCertificate() != null) {
                new C00371(sslError, this.mUrl, sslErrorHandler).start();
            }
        }

        public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
            if (this.mLastPageStartedUrl != null && this.mLastPageStartedUrl.equals(str)) {
                this.val$webView.onLocationChange(str);
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            this.mNoCompleteForUrl = str2;
            if (i == -10) {
                this.val$webView.onLoadError(str2, "[" + i + "] " + str + ": " + str2, AndroidWebView.ERROR_PROTOCOL_UNSUPPORTED);
            } else {
                this.val$webView.onLoadError(str2, "[" + i + "] " + str + ": " + str2, AndroidWebView.ERROR_OTHER);
            }
        }
    }

    /* renamed from: com.adobe.air.AndroidWebView.3 */
    class C00393 implements Runnable {
        C00393() {
        }

        public void run() {
            if (AndroidWebView.this.mView != null) {
                LayoutParams layoutParams = new RelativeLayout.LayoutParams(AndroidWebView.this.mGlobalBounds.width(), AndroidWebView.this.mGlobalBounds.height());
                layoutParams.leftMargin = AndroidWebView.this.mGlobalBounds.left;
                layoutParams.topMargin = AndroidWebView.this.mGlobalBounds.top;
                AndroidWebView.this.mView.setLayoutParams(layoutParams);
                AndroidWebView.this.mView.requestLayout();
            }
        }
    }

    /* renamed from: com.adobe.air.AndroidWebView.4 */
    static /* synthetic */ class C00404 {
        static final /* synthetic */ int[] $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState;

        static {
            $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState = new int[ActivityState.values().length];
            try {
                $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState[ActivityState.RESUMED.ordinal()] = AndroidWebView.FOCUS_NONE;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState[ActivityState.PAUSED.ordinal()] = AndroidWebView.FOCUS_UP;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState[ActivityState.STARTED.ordinal()] = AndroidWebView.FOCUS_DOWN;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState[ActivityState.DESTROYED.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public class AndroidWebViewImpl extends WebView {
        private int mLastFocusDirection;
        private View m_focusedChildView;
        private boolean m_hasFocus;
        private boolean m_inRequestChildFocus;

        public AndroidWebViewImpl(Context context) {
            super(context);
            this.m_inRequestChildFocus = false;
            this.m_focusedChildView = null;
            this.m_hasFocus = false;
            this.mLastFocusDirection = AndroidWebView.ERROR_OTHER;
        }

        public AndroidWebViewImpl(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            this.m_inRequestChildFocus = false;
            this.m_focusedChildView = null;
            this.m_hasFocus = false;
            this.mLastFocusDirection = AndroidWebView.ERROR_OTHER;
        }

        public AndroidWebViewImpl(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.m_inRequestChildFocus = false;
            this.m_focusedChildView = null;
            this.m_hasFocus = false;
            this.mLastFocusDirection = AndroidWebView.ERROR_OTHER;
        }

        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            if (!this.m_hasFocus) {
                requestFocus();
            }
            return super.dispatchTouchEvent(motionEvent);
        }

        public void requestChildFocus(View view, View view2) {
            this.m_inRequestChildFocus = true;
            if (!this.m_hasFocus) {
                setRealFocus(true);
            }
            try {
                this.m_focusedChildView = view;
                super.requestChildFocus(view, view2);
            } finally {
                this.m_inRequestChildFocus = false;
            }
        }

        public void clearChildFocus(View view) {
            this.m_inRequestChildFocus = true;
            try {
                super.clearChildFocus(view);
            } finally {
                this.m_inRequestChildFocus = false;
            }
        }

        public void clearFocus() {
            if (this.m_focusedChildView != null) {
                this.m_focusedChildView.clearFocus();
                if (AutoCompleteTextView.class.isInstance(this.m_focusedChildView)) {
                    AndroidWebView.this.mView.removeView(this.m_focusedChildView);
                }
                this.m_focusedChildView = null;
                setRealFocus(false);
                return;
            }
            super.clearFocus();
        }

        private void setRealFocus(boolean z) {
            super.onFocusChanged(z, AndroidWebView.ERROR_OTHER, new Rect());
            invalidate();
            dispatchFocusEvent(z, AndroidWebView.ERROR_OTHER);
        }

        public boolean isInTextEditingMode() {
            return this.m_hasFocus && !(this.m_focusedChildView == null && getChildCount() == 0);
        }

        private void dispatchFocusEvent(boolean z, int i) {
            int i2 = AndroidWebView.FOCUS_UP;
            if (this.m_hasFocus != z) {
                this.m_hasFocus = z;
                AndroidWebView androidWebView = AndroidWebView.this;
                if (androidWebView.mInternalReference != 0) {
                    if (AndroidWebView.this.mAIRSurface != null) {
                        AndroidWebView.this.mAIRSurface.updateFocusedStageWebView(androidWebView, this.m_hasFocus);
                    }
                    if ((i & AndroidWebView.FOCUS_UP) == AndroidWebView.FOCUS_UP) {
                        i2 = AndroidWebView.FOCUS_DOWN;
                    } else if ((i & AndroidWebView.FOCUS_NONE) != AndroidWebView.FOCUS_NONE) {
                        i2 = AndroidWebView.FOCUS_NONE;
                    }
                    if (z) {
                        androidWebView.dispatchFocusIn(AndroidWebView.this.mInternalReference, i2);
                    } else {
                        androidWebView.dispatchFocusOut(AndroidWebView.this.mInternalReference, i2);
                    }
                }
            }
        }

        protected void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            if (!this.m_inRequestChildFocus || this.m_focusedChildView == null || !this.m_hasFocus) {
                if (i == 0) {
                    i = this.mLastFocusDirection;
                }
                this.mLastFocusDirection = AndroidWebView.ERROR_OTHER;
                dispatchFocusEvent(z, i);
            }
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            boolean dispatchKeyEvent = super.dispatchKeyEvent(keyEvent);
            if (!dispatchKeyEvent && keyEvent.getAction() == 0) {
                int i = AndroidWebView.ERROR_OTHER;
                if (keyEvent.getKeyCode() == 19) {
                    i = 33;
                } else if (keyEvent.getKeyCode() == 20) {
                    i = 130;
                }
                if (i != 0) {
                    AndroidWebView androidWebView = AndroidWebView.this;
                    this.mLastFocusDirection = i;
                    androidWebView.setStageFocus(i);
                    return true;
                }
            }
            if (dispatchKeyEvent) {
                return dispatchKeyEvent;
            }
            return AndroidWebView.this.mAIRSurface.dispatchKeyEvent(keyEvent);
        }
    }

    public class WebViewCustomView {
        private CustomViewCallback mCallback;
        private FrameLayout mCustomViewHolder;

        /* renamed from: com.adobe.air.AndroidWebView.WebViewCustomView.1 */
        class C00411 extends FrameLayout {
            C00411(Context context) {
                super(context);
            }

            public boolean dispatchKeyEvent(KeyEvent keyEvent) {
                if (super.dispatchKeyEvent(keyEvent)) {
                    return true;
                }
                if (keyEvent.getAction() != 0 || keyEvent.getKeyCode() != 4) {
                    return false;
                }
                WebViewCustomView.this.onHideCustomView();
                return true;
            }
        }

        public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
            if (this.mCallback != null) {
                customViewCallback.onCustomViewHidden();
                return;
            }
            this.mCallback = customViewCallback;
            View view2 = AndroidActivityWrapper.GetAndroidActivityWrapper().getView();
            view2.setVisibility(8);
            View overlaysLayout = AndroidActivityWrapper.GetAndroidActivityWrapper().getOverlaysLayout(false);
            if (overlaysLayout != null) {
                overlaysLayout.setVisibility(8);
            }
            this.mCustomViewHolder = new C00411(AndroidWebView.this.mContext);
            this.mCustomViewHolder.setBackgroundColor(-16777216);
            ((ViewGroup) view2.getParent()).addView(this.mCustomViewHolder, new FrameLayout.LayoutParams(-1, -1));
            this.mCustomViewHolder.addView(view, new FrameLayout.LayoutParams(-1, -1, 17));
            this.mCustomViewHolder.bringToFront();
            this.mCustomViewHolder.requestFocus();
        }

        public void onHideCustomView() {
            if (this.mCallback != null && this.mCustomViewHolder != null) {
                AndroidActivityWrapper.GetAndroidActivityWrapper().getView().setVisibility(AndroidWebView.ERROR_OTHER);
                View overlaysLayout = AndroidActivityWrapper.GetAndroidActivityWrapper().getOverlaysLayout(false);
                if (overlaysLayout != null) {
                    overlaysLayout.setVisibility(AndroidWebView.ERROR_OTHER);
                }
                ((ViewGroup) this.mCustomViewHolder.getParent()).removeView(this.mCustomViewHolder);
                this.mCustomViewHolder = null;
                this.mCallback.onCustomViewHidden();
                this.mCallback = null;
            }
        }
    }

    private native void dispatchFocusIn(long j, int i);

    private native void dispatchFocusOut(long j, int i);

    private native void dispatchLoadComplete(long j);

    private native void dispatchLoadError(long j, String str, int i);

    private native boolean dispatchLocationChange(long j);

    private native boolean dispatchLocationChanging(long j, String str);

    public AndroidWebView() {
        this.mBounds = null;
        this.mGlobalBounds = null;
        this.mUrl = null;
        this.mInternalReference = 0;
        this.mContext = AndroidActivityWrapper.GetAndroidActivityWrapper().getActivity();
        this.mView = new AndroidWebViewImpl(this.mContext);
        WebSettings settings = this.mView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setNeedInitialFocus(true);
        if (VERSION.SDK_INT < 18) {
            settings.setPluginState(PluginState.ON);
        }
        if (VERSION.SDK_INT > 15) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
        this.mView.setScrollbarFadingEnabled(true);
        this.mView.setScrollBarStyle(CompanionView.kTouchMetaStateIsPen);
        this.mView.setWebChromeClient(new C00361());
        this.mView.setWebViewClient(new C00382(this));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityStateChanged(com.adobe.air.AndroidActivityWrapper.ActivityState r6) {
        /*
        r5 = this;
        r0 = android.webkit.WebView.class;
        r1 = "isPaused";
        r2 = 0;
        r2 = new java.lang.Class[r2];	 Catch:{ Exception -> 0x0057 }
        r0 = r0.getMethod(r1, r2);	 Catch:{ Exception -> 0x0057 }
        r1 = android.webkit.WebView.class;
        r2 = "onResume";
        r3 = 0;
        r3 = new java.lang.Class[r3];	 Catch:{ Exception -> 0x0057 }
        r1 = r1.getMethod(r2, r3);	 Catch:{ Exception -> 0x0057 }
        r2 = android.webkit.WebView.class;
        r3 = "onPause";
        r4 = 0;
        r4 = new java.lang.Class[r4];	 Catch:{ Exception -> 0x0057 }
        r2 = r2.getMethod(r3, r4);	 Catch:{ Exception -> 0x0057 }
        r3 = r5.mView;	 Catch:{ Exception -> 0x0057 }
        r4 = 0;
        r4 = new java.lang.Object[r4];	 Catch:{ Exception -> 0x0057 }
        r0 = r0.invoke(r3, r4);	 Catch:{ Exception -> 0x0057 }
        r0 = (java.lang.Boolean) r0;	 Catch:{ Exception -> 0x0057 }
        r0 = r0.booleanValue();	 Catch:{ Exception -> 0x0057 }
        r3 = com.adobe.air.AndroidWebView.C00404.$SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState;	 Catch:{ Exception -> 0x0057 }
        r4 = r6.ordinal();	 Catch:{ Exception -> 0x0057 }
        r3 = r3[r4];	 Catch:{ Exception -> 0x0057 }
        switch(r3) {
            case 1: goto L_0x0047;
            case 2: goto L_0x0059;
            default: goto L_0x003b;
        };
    L_0x003b:
        r0 = com.adobe.air.AndroidWebView.C00404.$SwitchMap$com$adobe$air$AndroidActivityWrapper$ActivityState;
        r1 = r6.ordinal();
        r0 = r0[r1];
        switch(r0) {
            case 3: goto L_0x0069;
            case 4: goto L_0x0075;
            default: goto L_0x0046;
        };
    L_0x0046:
        return;
    L_0x0047:
        if (r0 == 0) goto L_0x003b;
    L_0x0049:
        r0 = r5.mView;	 Catch:{ Exception -> 0x0057 }
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ Exception -> 0x0057 }
        r1.invoke(r0, r2);	 Catch:{ Exception -> 0x0057 }
        r0 = r5.mView;	 Catch:{ Exception -> 0x0057 }
        r0.resumeTimers();	 Catch:{ Exception -> 0x0057 }
        goto L_0x003b;
    L_0x0057:
        r0 = move-exception;
        goto L_0x003b;
    L_0x0059:
        if (r0 != 0) goto L_0x003b;
    L_0x005b:
        r0 = r5.mView;	 Catch:{ Exception -> 0x0057 }
        r0.pauseTimers();	 Catch:{ Exception -> 0x0057 }
        r0 = r5.mView;	 Catch:{ Exception -> 0x0057 }
        r1 = 0;
        r1 = new java.lang.Object[r1];	 Catch:{ Exception -> 0x0057 }
        r2.invoke(r0, r1);	 Catch:{ Exception -> 0x0057 }
        goto L_0x003b;
    L_0x0069:
        r0 = r5.mCustomViewHolder;
        if (r0 != 0) goto L_0x0046;
    L_0x006d:
        r0 = new com.adobe.air.AndroidWebView$WebViewCustomView;
        r0.<init>();
        r5.mCustomViewHolder = r0;
        goto L_0x0046;
    L_0x0075:
        r0 = r5.mCustomViewHolder;
        if (r0 == 0) goto L_0x0046;
    L_0x0079:
        r0 = r5.mCustomViewHolder;
        r0.onHideCustomView();
        r0 = 0;
        r5.mCustomViewHolder = r0;
        goto L_0x0046;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.air.AndroidWebView.onActivityStateChanged(com.adobe.air.AndroidActivityWrapper$ActivityState):void");
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void setInternalReference(long j) {
        this.mInternalReference = j;
    }

    public void destroyInternals() {
        removedFromStage();
        this.mInternalReference = 0;
        this.mView.destroy();
        this.mView = null;
    }

    public void stop() {
        this.mView.stopLoading();
    }

    public void reload() {
        this.mView.reload();
    }

    public void goBack() {
        this.mView.goBack();
    }

    public void goForward() {
        this.mView.goForward();
    }

    public boolean canGoBack() {
        return this.mView.canGoBack();
    }

    public boolean canGoForward() {
        return this.mView.canGoForward();
    }

    public void addedToStage(AIRWindowSurfaceView aIRWindowSurfaceView) {
        if (this.mLayout != null) {
            removedFromStage();
        }
        this.mAIRSurface = aIRWindowSurfaceView;
        AndroidActivityWrapper activityWrapper = aIRWindowSurfaceView.getActivityWrapper();
        activityWrapper.addActivityStateChangeListner(this);
        this.mLayout = activityWrapper.getOverlaysLayout(true);
        this.mLayout.addView(this.mView, new RelativeLayout.LayoutParams(ERROR_OTHER, ERROR_OTHER));
    }

    public void removedFromStage() {
        if (this.mLayout != null) {
            this.mLayout.removeView(this.mView);
            this.mLayout = null;
            AndroidActivityWrapper activityWrapper = this.mAIRSurface.getActivityWrapper();
            activityWrapper.didRemoveOverlay();
            activityWrapper.removeActivityStateChangeListner(this);
            this.mAIRSurface.updateFocusedStageWebView(this, false);
        }
        this.mAIRSurface = null;
    }

    public void setVisibility(boolean z) {
        int i = z ? ERROR_OTHER : 4;
        if (this.mView.getVisibility() != i) {
            this.mView.setVisibility(i);
        }
    }

    public long updateViewBoundsWithKeyboard(int i) {
        int i2 = ERROR_OTHER;
        this.mGlobalBounds = this.mBounds;
        if (this.mAIRSurface != null && isInTextEditingMode()) {
            Rect rect = new Rect(ERROR_OTHER, ERROR_OTHER, this.mAIRSurface.getVisibleBoundWidth(), this.mAIRSurface.getVisibleBoundHeight());
            if (!rect.contains(this.mBounds)) {
                int min = Math.min(Math.max(ERROR_OTHER, this.mBounds.top), i);
                i2 = Math.min(Math.max(ERROR_OTHER, this.mBounds.bottom), i);
                if (min == i2) {
                    return 0;
                }
                i2 -= rect.bottom;
                if (i2 <= 0) {
                    return 0;
                }
                if (i2 > min) {
                    this.mGlobalBounds = new Rect(this.mBounds);
                    this.mGlobalBounds.bottom = rect.bottom + min;
                    i2 = min;
                }
            }
        }
        refreshGlobalBounds();
        return (long) i2;
    }

    public void resetGlobalBounds() {
        this.mGlobalBounds = this.mBounds;
        refreshGlobalBounds();
    }

    private void refreshGlobalBounds() {
        if (this.mView != null) {
            this.mView.post(new C00393());
        }
    }

    public void adjustViewBounds(double d, double d2, double d3, double d4) {
        this.mBounds = new Rect((int) d, (int) d2, (int) (d + d3), (int) (d2 + d4));
        this.mGlobalBounds = this.mBounds;
        refreshGlobalBounds();
    }

    public void loadString(String str, String str2) {
        Object obj = FOCUS_NONE;
        try {
            byte[] bArr;
            Object bytes = str.getBytes("utf-8");
            if (!(bytes.length >= FOCUS_DOWN && bytes[ERROR_OTHER] == 239 && bytes[FOCUS_NONE] == 187 && bytes[FOCUS_UP] == 191)) {
                obj = ERROR_OTHER;
            }
            if (obj == null) {
                bArr = new byte[(bytes.length + FOCUS_DOWN)];
                System.arraycopy(bytes, ERROR_OTHER, bArr, FOCUS_DOWN, bytes.length);
                bArr[ERROR_OTHER] = (byte) -17;
                bArr[FOCUS_NONE] = (byte) -69;
                bArr[FOCUS_UP] = (byte) -65;
            } else {
                obj = bytes;
            }
            this.mView.loadData(Base64.encodeToString(bArr, ERROR_OTHER, bArr.length, FOCUS_UP), "text/html", "base64");
        } catch (UnsupportedEncodingException e) {
        }
    }

    public void loadURL(String str) {
        if (str != null) {
            this.mView.loadUrl(str);
        }
    }

    public boolean onLocationChange(String str) {
        if (this.mInternalReference == 0) {
            return true;
        }
        this.mUrl = str;
        return dispatchLocationChange(this.mInternalReference);
    }

    private String decodeURL(String str) {
        try {
            return new String(URLUtil.decode(str.getBytes()));
        } catch (IllegalArgumentException e) {
            return str;
        }
    }

    public boolean onLocationChanging(String str) {
        if (this.mInternalReference == 0) {
            return true;
        }
        return dispatchLocationChanging(this.mInternalReference, decodeURL(str));
    }

    public void onLoadComplete(String str) {
        if (this.mInternalReference != 0) {
            this.mUrl = str;
            dispatchLoadComplete(this.mInternalReference);
        }
    }

    public void onLoadError(String str, String str2, int i) {
        if (this.mInternalReference != 0) {
            this.mUrl = str;
            dispatchLoadError(this.mInternalReference, str2, i);
        }
    }

    public String getCurrentLocation() {
        String str = this.mUrl;
        if (str == null) {
            return "about:blank";
        }
        return decodeURL(str);
    }

    public String getPageTitle() {
        String title = this.mView.getTitle();
        if (title == null) {
            return "";
        }
        return title;
    }

    public void assignFocus(int i) {
        switch (i) {
            case FOCUS_NONE /*1*/:
                this.mView.requestFocus();
            case FOCUS_UP /*2*/:
                this.mView.pageUp(true);
                this.mView.requestFocus(33);
            case FOCUS_DOWN /*3*/:
                this.mView.pageDown(true);
                this.mView.requestFocus(130);
            default:
        }
    }

    public void clearFocus() {
        this.mView.clearFocus();
        this.mAIRSurface.requestFocus();
    }

    public void setStageFocus(int i) {
        this.mView.clearFocus();
        this.mAIRSurface.requestFocus(i);
    }

    public Bitmap captureSnapshot(int i, int i2) {
        Bitmap bitmap = null;
        if (i >= 0 && i2 >= 0 && !(i == 0 && i2 == 0)) {
            bitmap = Bitmap.createBitmap(i, i2, Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.translate((float) (-this.mView.getScrollX()), (float) (-this.mView.getScrollY()));
            boolean isHorizontalScrollBarEnabled = this.mView.isHorizontalScrollBarEnabled();
            boolean isVerticalScrollBarEnabled = this.mView.isVerticalScrollBarEnabled();
            this.mView.setHorizontalScrollBarEnabled(false);
            this.mView.setVerticalScrollBarEnabled(false);
            try {
                this.mView.draw(canvas);
            } catch (Exception e) {
                Picture capturePicture = this.mView.capturePicture();
                float scale = this.mView.getScale();
                canvas.scale(scale, scale);
                capturePicture.draw(canvas);
            }
            this.mView.setHorizontalScrollBarEnabled(isHorizontalScrollBarEnabled);
            this.mView.setVerticalScrollBarEnabled(isVerticalScrollBarEnabled);
        }
        return bitmap;
    }

    public boolean isInTextEditingMode() {
        return this.mView.isInTextEditingMode();
    }
}
