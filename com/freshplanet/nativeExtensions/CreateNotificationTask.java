package com.freshplanet.nativeExtensions;

import android.app.Notification;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.RemoteViews;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class CreateNotificationTask extends AsyncTask<URL, Integer, Long> {
    private Bitmap _bitmap;
    private RemoteViews _contentView;
    private int _customLayoutImageContainer;
    private NotificationManager _nm;
    private Notification _notif;
    private int _notifyId;

    public void setParams(int customLayoutImageContainer, int notifyId, NotificationManager _notifMan, Notification notif, RemoteViews contentView) {
        this._customLayoutImageContainer = customLayoutImageContainer;
        this._notifyId = notifyId;
        this._notif = notif;
        this._nm = _notifMan;
        this._contentView = contentView;
    }

    protected Long doInBackground(URL... urls) {
        try {
            HttpURLConnection connection = (HttpURLConnection) urls[0].openConnection();
            connection.setDoInput(true);
            connection.connect();
            this._bitmap = BitmapFactory.decodeStream(connection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(Long result) {
        this._contentView.setImageViewBitmap(this._customLayoutImageContainer, this._bitmap);
        this._notif.contentView = this._contentView;
        this._nm.notify(this._notifyId, this._notif);
    }
}
