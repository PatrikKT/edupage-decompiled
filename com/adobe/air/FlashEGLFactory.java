package com.adobe.air;

import android.os.Build.VERSION;
import com.adobe.air.wand.view.GestureListener;

public class FlashEGLFactory {

    /* renamed from: com.adobe.air.FlashEGLFactory.1 */
    static /* synthetic */ class C00451 {
        static final /* synthetic */ int[] $SwitchMap$com$adobe$air$FlashEGLFactory$FlashEGLType;

        static {
            $SwitchMap$com$adobe$air$FlashEGLFactory$FlashEGLType = new int[FlashEGLType.values().length];
            try {
                $SwitchMap$com$adobe$air$FlashEGLFactory$FlashEGLType[FlashEGLType.FLASHEGL14.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$adobe$air$FlashEGLFactory$FlashEGLType[FlashEGLType.FLASHEGL10.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public enum FlashEGLType {
        FLASHEGL10,
        FLASHEGL14
    }

    private FlashEGLFactory() {
    }

    public static FlashEGL CreateFlashEGL() {
        FlashEGLType flashEGLType = FlashEGLType.FLASHEGL10;
        if (VERSION.SDK_INT >= 21 || VERSION.CODENAME.equals("L")) {
            flashEGLType = FlashEGLType.FLASHEGL14;
        }
        return CreateFlashEGL(flashEGLType);
    }

    public static FlashEGL CreateFlashEGL(FlashEGLType flashEGLType) {
        switch (C00451.$SwitchMap$com$adobe$air$FlashEGLFactory$FlashEGLType[flashEGLType.ordinal()]) {
            case GestureListener.kGestureUpdate /*1*/:
                return new FlashEGL10();
            default:
                return new FlashEGL10();
        }
    }
}
