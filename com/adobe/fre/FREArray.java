package com.adobe.fre;

public class FREArray extends FREObject {
    public native long getLength() throws FREInvalidObjectException, FREWrongThreadException;

    public native FREObject getObjectAt(long j) throws FREInvalidObjectException, IllegalArgumentException, FREWrongThreadException;

    public native void setLength(long j) throws FREInvalidObjectException, IllegalArgumentException, FREReadOnlyException, FREWrongThreadException;

    public native void setObjectAt(long j, FREObject fREObject) throws FREInvalidObjectException, FRETypeMismatchException, FREWrongThreadException;

    private FREArray(CFREObjectWrapper obj) {
        super(obj);
    }

    protected FREArray(String base, FREObject[] constructorArgs) throws FRETypeMismatchException, FREInvalidObjectException, FREASErrorException, FRENoSuchNameException, FREWrongThreadException {
        super("Vector.<" + base + ">", constructorArgs);
    }

    protected FREArray(FREObject[] constructorArgs) throws FRETypeMismatchException, FREInvalidObjectException, FREASErrorException, FRENoSuchNameException, FREWrongThreadException {
        super("Array", constructorArgs);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.adobe.fre.FREArray newArray(java.lang.String r3, int r4, boolean r5) throws com.adobe.fre.FREASErrorException, com.adobe.fre.FRENoSuchNameException, com.adobe.fre.FREWrongThreadException, java.lang.IllegalStateException {
        /*
        r1 = 2;
        r0 = new com.adobe.fre.FREObject[r1];
        r1 = 0;
        r2 = new com.adobe.fre.FREObject;
        r2.<init>(r4);
        r0[r1] = r2;
        r1 = 1;
        r2 = new com.adobe.fre.FREObject;
        r2.<init>(r5);
        r0[r1] = r2;
        r1 = new com.adobe.fre.FREArray;	 Catch:{ FRETypeMismatchException -> 0x001c, FREInvalidObjectException -> 0x0019 }
        r1.<init>(r3, r0);	 Catch:{ FRETypeMismatchException -> 0x001c, FREInvalidObjectException -> 0x0019 }
    L_0x0018:
        return r1;
    L_0x0019:
        r1 = move-exception;
    L_0x001a:
        r1 = 0;
        goto L_0x0018;
    L_0x001c:
        r1 = move-exception;
        goto L_0x001a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.fre.FREArray.newArray(java.lang.String, int, boolean):com.adobe.fre.FREArray");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.adobe.fre.FREArray newArray(int r3) throws com.adobe.fre.FREASErrorException, com.adobe.fre.FREWrongThreadException, java.lang.IllegalStateException {
        /*
        r1 = 1;
        r0 = new com.adobe.fre.FREObject[r1];
        r1 = 0;
        r2 = new com.adobe.fre.FREObject;
        r2.<init>(r3);
        r0[r1] = r2;
        r1 = new com.adobe.fre.FREArray;	 Catch:{ FRETypeMismatchException -> 0x0016, FREInvalidObjectException -> 0x0014, FRENoSuchNameException -> 0x0011 }
        r1.<init>(r0);	 Catch:{ FRETypeMismatchException -> 0x0016, FREInvalidObjectException -> 0x0014, FRENoSuchNameException -> 0x0011 }
    L_0x0010:
        return r1;
    L_0x0011:
        r1 = move-exception;
    L_0x0012:
        r1 = 0;
        goto L_0x0010;
    L_0x0014:
        r1 = move-exception;
        goto L_0x0012;
    L_0x0016:
        r1 = move-exception;
        goto L_0x0012;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.fre.FREArray.newArray(int):com.adobe.fre.FREArray");
    }
}
