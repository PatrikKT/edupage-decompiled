package com.adobe.air;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class AndroidIdleState {
    public static final int IDLE_STATE_NORMAL = 0;
    private static final String IDLE_STATE_TAG = "AndroidIdleState";
    public static final int IDLE_STATE_WAKEUP = 1;
    private static AndroidIdleState mIdleStateManager;
    private int mCurrentIdleState;
    private boolean mIsWakeUpLockHeld;
    private KeyguardLock mKeyGuardLock;
    private WakeLock mScreenBrightLock;
    private BroadcastReceiver sReceiver;
    private boolean sScreenOn;

    /* renamed from: com.adobe.air.AndroidIdleState.1 */
    class C00301 extends BroadcastReceiver {
        C00301() {
        }

        public void onReceive(Context context, Intent intent) {
            boolean z = true;
            if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                z = false;
            } else if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
            }
            if (AndroidIdleState.this.sScreenOn != z) {
                AndroidIdleState.this.sScreenOn = z;
                AndroidActivityWrapper.GetAndroidActivityWrapper().onScreenStateChanged(AndroidIdleState.this.sScreenOn);
            }
        }
    }

    static {
        mIdleStateManager = null;
    }

    public static AndroidIdleState GetIdleStateManager(Context context) {
        if (mIdleStateManager == null) {
            mIdleStateManager = new AndroidIdleState(context);
        }
        return mIdleStateManager;
    }

    private AndroidIdleState(Context context) {
        this.mScreenBrightLock = null;
        this.mKeyGuardLock = null;
        this.mIsWakeUpLockHeld = false;
        this.mCurrentIdleState = IDLE_STATE_NORMAL;
        this.sReceiver = null;
        this.sScreenOn = true;
        if (this.sReceiver == null) {
            try {
                this.sReceiver = new C00301();
                IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                context.registerReceiver(this.sReceiver, intentFilter);
            } catch (Exception e) {
            }
        }
    }

    public void ChangeIdleState(Context context, int i) {
        if (i == 0) {
            try {
                releaseLock();
                this.mCurrentIdleState = IDLE_STATE_NORMAL;
                return;
            } catch (Exception e) {
                return;
            }
        }
        if (this.mScreenBrightLock == null) {
            try {
                this.mScreenBrightLock = ((PowerManager) context.getSystemService("power")).newWakeLock(268435466, "DoNotDimScreen");
                this.mKeyGuardLock = ((KeyguardManager) context.getSystemService("keyguard")).newKeyguardLock("DoNotLockKeys");
            } catch (Exception e2) {
                this.mScreenBrightLock = null;
                this.mKeyGuardLock = null;
                return;
            }
        }
        this.mCurrentIdleState = IDLE_STATE_WAKEUP;
        acquireLock();
    }

    public void acquireLock() {
        try {
            if (this.mCurrentIdleState == IDLE_STATE_WAKEUP && !this.mIsWakeUpLockHeld) {
                this.mScreenBrightLock.acquire();
                this.mKeyGuardLock.disableKeyguard();
                this.mIsWakeUpLockHeld = true;
            }
        } catch (Exception e) {
        }
    }

    public void releaseLock() {
        try {
            if (this.mCurrentIdleState == IDLE_STATE_WAKEUP && this.mIsWakeUpLockHeld) {
                this.mScreenBrightLock.release();
                this.mKeyGuardLock.reenableKeyguard();
                this.mIsWakeUpLockHeld = false;
            }
        } catch (Exception e) {
        }
    }
}
