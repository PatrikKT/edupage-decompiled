package com.freshplanet.nativeExtensions;

import android.util.Log;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import java.util.HashMap;
import java.util.Map;

public class C2DMExtensionContext extends FREContext {
    private static String TAG;

    static {
        TAG = "c2dmContext";
    }

    public C2DMExtensionContext() {
        Log.d(TAG, "C2DMExtensionContext.C2DMExtensionContext");
    }

    public void dispose() {
        Log.d(TAG, "C2DMExtensionContext.dispose");
        C2DMExtension.context = null;
    }

    public Map<String, FREFunction> getFunctions() {
        Log.d(TAG, "C2DMExtensionContext.getFunctions");
        Map<String, FREFunction> functionMap = new HashMap();
        functionMap.put("registerPush", new C2DMRegisterFunction());
        functionMap.put("setBadgeNb", new SetBadgeValueFunction());
        functionMap.put("sendLocalNotification", new LocalNotificationFunction());
        functionMap.put("setIsAppInForeground", new SetIsAppInForegroundFunction());
        functionMap.put("resetCounters", new ResetCountersFunction());
        return functionMap;
    }
}
