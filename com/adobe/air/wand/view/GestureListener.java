package com.adobe.air.wand.view;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.ViewConfiguration;

public class GestureListener implements OnGestureListener, OnDoubleTapListener, OnScaleGestureListener {
    private static final String LOG_TAG = "GestureListener";
    private static final int MAX_TOUCH_POINTS = 2;
    private static final float MM_PER_INCH = 25.4f;
    private static final float _FP_GESTURE_PAN_THRESHOLD_MM = 3.0f;
    private static final float _FP_GESTURE_ROTATION_THRESHOLD_DEGREES = 15.0f;
    private static final float _FP_GESTURE_SWIPE_PRIMARY_AXIS_MIN_MM = 10.0f;
    private static final float _FP_GESTURE_SWIPE_SECONDARY_AXIS_MAX_MM = 5.0f;
    private static final float _FP_GESTURE_ZOOM_PER_AXIS_THRESHOLD_MM = 3.0f;
    private static final float _FP_GESTURE_ZOOM_THRESHOLD_MM = 8.0f;
    public static final int kGestureAll = 8;
    public static final int kGestureBegin = 2;
    public static final int kGestureEnd = 4;
    public static final int kGesturePan = 1;
    public static final int kGestureRotate = 2;
    public static final int kGestureSwipe = 4;
    public static final int kGestureTap = 3;
    public static final int kGestureUpdate = 1;
    public static final int kGestureZoom = 0;
    private static int screenPPI;
    private boolean mCheckForSwipe;
    private CompanionView mCompanionView;
    private int mCouldBeTwoFingerTap;
    private boolean mDidOccurTwoFingerGesture;
    private TouchPoint[] mDownTouchPoints;
    private boolean mInPanTransformGesture;
    private boolean mInRotateTransformGesture;
    private boolean mInZoomTransformGesture;
    private boolean mInZoomTransformGestureX;
    private boolean mInZoomTransformGestureY;
    private float mPreviousAbsoluteRotation;
    private float mPreviousPanLocX;
    private float mPreviousPanLocY;
    private float mPreviousRotateLocX;
    private float mPreviousRotateLocY;
    private float mPreviousZoomLocX;
    private float mPreviousZoomLocY;
    private TouchPoint mPrimaryPointOfTwoFingerTap;
    private TouchPoint mSecondaryPointOfTwoFingerTap;
    private long mTwoFingerTapStartTime;

    private class TouchPoint {
        private int pid;
        private float f9x;
        private float f10y;

        TouchPoint() {
            this.f9x = 0.0f;
            this.f10y = 0.0f;
            this.pid = 0;
        }

        TouchPoint(float f, float f2, int i) {
            this.f9x = f;
            this.f10y = f2;
            this.pid = i;
        }

        private void assign(float f, float f2, int i) {
            this.f9x = f;
            this.f10y = f2;
            this.pid = i;
        }
    }

    static {
        screenPPI = 0;
    }

    public GestureListener(Context context, CompanionView companionView) {
        int i = 0;
        this.mPreviousAbsoluteRotation = 0.0f;
        this.mInRotateTransformGesture = false;
        this.mInZoomTransformGesture = false;
        this.mInZoomTransformGestureX = false;
        this.mInZoomTransformGestureY = false;
        this.mInPanTransformGesture = false;
        this.mPreviousRotateLocX = 0.0f;
        this.mPreviousRotateLocY = 0.0f;
        this.mPreviousZoomLocX = 0.0f;
        this.mPreviousZoomLocY = 0.0f;
        this.mPreviousPanLocX = 0.0f;
        this.mPreviousPanLocY = 0.0f;
        this.mTwoFingerTapStartTime = 0;
        this.mDidOccurTwoFingerGesture = false;
        this.mCompanionView = null;
        this.mCouldBeTwoFingerTap = 0;
        this.mSecondaryPointOfTwoFingerTap = null;
        this.mPrimaryPointOfTwoFingerTap = null;
        this.mCheckForSwipe = true;
        this.mCompanionView = companionView;
        this.mDownTouchPoints = new TouchPoint[kGestureRotate];
        while (i < kGestureRotate) {
            this.mDownTouchPoints[i] = new TouchPoint();
            i += kGestureUpdate;
        }
        this.mSecondaryPointOfTwoFingerTap = new TouchPoint();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        screenPPI = (int) ((displayMetrics.ydpi + displayMetrics.xdpi) / 2.0f);
    }

    public TouchPoint getDownTouchPoint(int i) {
        if (i < 0 || i >= kGestureRotate) {
            return null;
        }
        return this.mDownTouchPoints[i];
    }

    public void setDownTouchPoint(float f, float f2, int i) {
        if (i >= 0 && i < kGestureRotate) {
            this.mDownTouchPoints[i].assign(f, f2, i);
        }
    }

    public void setCouldBeTwoFingerTap(int i) {
        this.mCouldBeTwoFingerTap = i;
        if (i == 0) {
            this.mTwoFingerTapStartTime = System.currentTimeMillis();
            this.mDidOccurTwoFingerGesture = false;
        }
    }

    public int getCouldBeTwoFingerTap() {
        return this.mCouldBeTwoFingerTap;
    }

    public void setSecondaryPointOfTwoFingerTap(float f, float f2, int i) {
        this.mSecondaryPointOfTwoFingerTap = new TouchPoint(f, f2, i);
    }

    public void setPrimaryPointOfTwoFingerTap(float f, float f2, int i) {
        this.mPrimaryPointOfTwoFingerTap = new TouchPoint(f, f2, i);
    }

    public void mayStartNewTransformGesture() {
        this.mInRotateTransformGesture = false;
        this.mInZoomTransformGesture = false;
        this.mInZoomTransformGestureX = false;
        this.mInZoomTransformGestureY = false;
        this.mInPanTransformGesture = false;
    }

    public boolean getCheckForSwipe() {
        return this.mCheckForSwipe;
    }

    public void setCheckForSwipe(boolean z) {
        this.mCheckForSwipe = z;
    }

    public boolean endTwoFingerGesture() {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode == kGestureRotate) {
            long currentTimeMillis = System.currentTimeMillis();
            if (!this.mDidOccurTwoFingerGesture && this.mCouldBeTwoFingerTap == kGestureTap && currentTimeMillis - this.mTwoFingerTapStartTime < ((long) ViewConfiguration.getTapTimeout())) {
                onTwoFingerTap();
            }
            endRotateGesture();
            endPanGesture();
        }
        return true;
    }

    private void endRotateGesture() {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode == kGestureRotate && this.mInRotateTransformGesture) {
            this.mCompanionView.onGestureListener(kGestureSwipe, kGestureRotate, true, this.mPreviousRotateLocX, this.mPreviousRotateLocY, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
            this.mInRotateTransformGesture = false;
        }
    }

    private void endZoomGesture() {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode == kGestureRotate && this.mInZoomTransformGesture) {
            this.mCompanionView.onGestureListener(kGestureSwipe, 0, true, this.mPreviousZoomLocX, this.mPreviousZoomLocY, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
            this.mInZoomTransformGesture = false;
            this.mInZoomTransformGestureX = false;
            this.mInZoomTransformGestureY = false;
        }
    }

    private void endPanGesture() {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode == kGestureRotate && this.mInPanTransformGesture) {
            this.mCompanionView.onGestureListener(kGestureSwipe, kGestureUpdate, true, this.mPreviousPanLocX, this.mPreviousPanLocY, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
            this.mInPanTransformGesture = false;
        }
    }

    public boolean onDown(MotionEvent motionEvent) {
        return true;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode != kGestureRotate) {
            return true;
        }
        float f3 = 0.0f;
        float f4 = 0.0f;
        float f5 = 0.0f;
        float rotation;
        if (motionEvent2.getPointerCount() == kGestureRotate) {
            int i = kGestureUpdate;
            float x = (motionEvent2.getX(0) + motionEvent2.getX(kGestureUpdate)) / 2.0f;
            float y = (motionEvent2.getY(0) + motionEvent2.getY(kGestureUpdate)) / 2.0f;
            TouchPoint[] touchPointArr = new TouchPoint[kGestureRotate];
            for (multitouchMode = 0; multitouchMode < kGestureRotate; multitouchMode += kGestureUpdate) {
                touchPointArr[multitouchMode] = new TouchPoint(motionEvent2.getX(multitouchMode), motionEvent2.getY(multitouchMode), motionEvent2.getPointerId(multitouchMode));
            }
            int access$100 = touchPointArr[0].pid;
            int access$1002 = touchPointArr[kGestureUpdate].pid;
            if (access$100 >= 0 && access$100 < kGestureRotate && access$1002 >= 0 && access$1002 < kGestureRotate) {
                if (!this.mInPanTransformGesture) {
                    rotation = getRotation(this.mDownTouchPoints[access$100], this.mDownTouchPoints[access$1002], touchPointArr[0], touchPointArr[kGestureUpdate]);
                    if (Math.abs(rotation) > 180.0f) {
                        if (rotation > 0.0f) {
                            rotation = (360.0f - rotation) * -1.0f;
                        } else {
                            rotation += 360.0f;
                        }
                    }
                    if (this.mInRotateTransformGesture || Math.abs(rotation) > _FP_GESTURE_ROTATION_THRESHOLD_DEGREES) {
                        if (!this.mInRotateTransformGesture) {
                            i = kGestureRotate;
                            this.mInRotateTransformGesture = true;
                            this.mPreviousAbsoluteRotation = 0.0f;
                            this.mDidOccurTwoFingerGesture = true;
                        }
                        f3 = rotation - this.mPreviousAbsoluteRotation;
                        if (Math.abs(f3) > 180.0f) {
                            float f6;
                            if (f3 > 0.0f) {
                                f6 = (360.0f - f3) * -1.0f;
                            } else {
                                f6 = 360.0f + f3;
                            }
                            f3 = f6;
                        }
                        this.mPreviousAbsoluteRotation = rotation;
                        this.mPreviousRotateLocX = x;
                        this.mPreviousRotateLocY = y;
                        this.mCompanionView.onGestureListener(i, kGestureRotate, true, x, y, 1.0f, 1.0f, f3, 0.0f, 0.0f);
                        f3 = 0.0f;
                    }
                }
                if (!(this.mInZoomTransformGesture || this.mInRotateTransformGesture)) {
                    if (isPanGesture(this.mDownTouchPoints[access$100], this.mDownTouchPoints[access$1002], touchPointArr[0], touchPointArr[kGestureUpdate])) {
                        if (!this.mInPanTransformGesture) {
                            i = kGestureRotate;
                            this.mInPanTransformGesture = true;
                            this.mDidOccurTwoFingerGesture = true;
                        }
                        f4 = -1.0f * f;
                        f5 = -1.0f * f2;
                        this.mPreviousPanLocX = x;
                        this.mPreviousPanLocY = y;
                        this.mCompanionView.onGestureListener(i, kGestureUpdate, true, x, y, 1.0f, 1.0f, f3, f4, f5);
                    } else if (this.mInPanTransformGesture) {
                        endPanGesture();
                        setDownTouchPoint(touchPointArr[0].f9x, touchPointArr[0].f10y, touchPointArr[0].pid);
                        setDownTouchPoint(touchPointArr[kGestureUpdate].f9x, touchPointArr[kGestureUpdate].f10y, touchPointArr[kGestureUpdate].pid);
                    }
                }
            }
        } else if (motionEvent2.getPointerCount() == kGestureUpdate) {
            multitouchMode = motionEvent2.getPointerId(0);
            if (multitouchMode >= 0 && multitouchMode < kGestureRotate && this.mCheckForSwipe && motionEvent.getPointerCount() == kGestureUpdate) {
                Object obj;
                float x2 = motionEvent2.getX(0) - this.mDownTouchPoints[multitouchMode].f9x;
                rotation = motionEvent2.getY(0) - this.mDownTouchPoints[multitouchMode].f10y;
                if ((Math.abs(x2) * MM_PER_INCH) / ((float) screenPPI) >= _FP_GESTURE_SWIPE_PRIMARY_AXIS_MIN_MM && (Math.abs(rotation) * MM_PER_INCH) / ((float) screenPPI) <= _FP_GESTURE_SWIPE_SECONDARY_AXIS_MAX_MM) {
                    f5 = 0.0f;
                    f4 = x2 > 0.0f ? 1.0f : -1.0f;
                    obj = kGestureUpdate;
                } else if ((Math.abs(rotation) * MM_PER_INCH) / ((float) screenPPI) < _FP_GESTURE_SWIPE_PRIMARY_AXIS_MIN_MM || (Math.abs(x2) * MM_PER_INCH) / ((float) screenPPI) > _FP_GESTURE_SWIPE_SECONDARY_AXIS_MAX_MM) {
                    obj = null;
                } else {
                    f4 = 0.0f;
                    f5 = rotation > 0.0f ? 1.0f : -1.0f;
                    obj = kGestureUpdate;
                }
                if (obj != null) {
                    this.mCompanionView.onGestureListener(kGestureAll, kGestureSwipe, true, motionEvent.getX(0), motionEvent2.getY(0), 1.0f, 1.0f, 0.0f, f4, f5);
                    this.mCheckForSwipe = false;
                }
            }
        }
        return true;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onTwoFingerTap() {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode != kGestureRotate) {
            return true;
        }
        this.mCompanionView.onGestureListener(kGestureAll, kGestureTap, false, (this.mSecondaryPointOfTwoFingerTap.f9x + this.mPrimaryPointOfTwoFingerTap.f9x) / 2.0f, (this.mSecondaryPointOfTwoFingerTap.f10y + this.mPrimaryPointOfTwoFingerTap.f10y) / 2.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
        this.mCouldBeTwoFingerTap = 0;
        return true;
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return true;
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return true;
    }

    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return true;
    }

    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        if (this.mInZoomTransformGesture) {
            endZoomGesture();
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onScale(android.view.ScaleGestureDetector r25) {
        /*
        r24 = this;
        r0 = r24;
        r2 = r0.mCompanionView;
        r2 = r2.getMultitouchMode();
        r0 = r24;
        r3 = r0.mCompanionView;
        r3.getClass();
        r3 = 2;
        if (r2 == r3) goto L_0x0014;
    L_0x0012:
        r2 = 1;
    L_0x0013:
        return r2;
    L_0x0014:
        r3 = 1;
        r4 = 0;
        r6 = r25.getFocusX();
        r7 = r25.getFocusY();
        r2 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r9 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r10 = 0;
        r11 = 0;
        r12 = 0;
        r5 = r25.getPreviousSpan();
        r0 = (double) r5;
        r18 = r0;
        r5 = r25.getCurrentSpan();
        r14 = (double) r5;
        r14 = r14 - r18;
        r20 = java.lang.Math.abs(r14);
        r16 = 0;
        r14 = 0;
        r5 = android.os.Build.VERSION.SDK_INT;
        r8 = 11;
        if (r5 < r8) goto L_0x005f;
    L_0x0041:
        r5 = r25.getCurrentSpanX();
        r8 = r25.getPreviousSpanX();
        r5 = r5 - r8;
        r5 = java.lang.Math.abs(r5);
        r0 = (double) r5;
        r16 = r0;
        r5 = r25.getCurrentSpanY();
        r8 = r25.getPreviousSpanY();
        r5 = r5 - r8;
        r5 = java.lang.Math.abs(r5);
        r14 = (double) r5;
    L_0x005f:
        r22 = 0;
        r5 = (r18 > r22 ? 1 : (r18 == r22 ? 0 : -1));
        if (r5 == 0) goto L_0x012f;
    L_0x0065:
        r0 = r24;
        r5 = r0.mInZoomTransformGesture;
        if (r5 != 0) goto L_0x007f;
    L_0x006b:
        r18 = 4627842681983205376; // 0x4039666660000000 float:3.6893488E19 double:25.399999618530273;
        r18 = r18 * r20;
        r5 = screenPPI;
        r0 = (double) r5;
        r20 = r0;
        r18 = r18 / r20;
        r20 = 4620693217682128896; // 0x4020000000000000 float:0.0 double:8.0;
        r5 = (r18 > r20 ? 1 : (r18 == r20 ? 0 : -1));
        if (r5 <= 0) goto L_0x012c;
    L_0x007f:
        r0 = r24;
        r5 = r0.mInZoomTransformGesture;
        if (r5 != 0) goto L_0x0090;
    L_0x0085:
        r3 = 1;
        r0 = r24;
        r0.mInZoomTransformGesture = r3;
        r3 = 2;
        r5 = 1;
        r0 = r24;
        r0.mDidOccurTwoFingerGesture = r5;
    L_0x0090:
        r5 = android.os.Build.VERSION.SDK_INT;
        r8 = 11;
        if (r5 < r8) goto L_0x0126;
    L_0x0096:
        r5 = r25.getPreviousSpanX();
        r8 = 0;
        r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1));
        if (r5 == 0) goto L_0x00d4;
    L_0x009f:
        r5 = r25.getCurrentSpanX();
        r8 = 0;
        r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1));
        if (r5 == 0) goto L_0x00d4;
    L_0x00a8:
        r0 = r24;
        r5 = r0.mInZoomTransformGestureX;
        if (r5 != 0) goto L_0x00c2;
    L_0x00ae:
        r18 = 4627842681983205376; // 0x4039666660000000 float:3.6893488E19 double:25.399999618530273;
        r16 = r16 * r18;
        r5 = screenPPI;
        r0 = (double) r5;
        r18 = r0;
        r16 = r16 / r18;
        r18 = 4613937818241073152; // 0x4008000000000000 float:0.0 double:3.0;
        r5 = (r16 > r18 ? 1 : (r16 == r18 ? 0 : -1));
        if (r5 <= 0) goto L_0x00d4;
    L_0x00c2:
        r2 = r25.getCurrentSpanX();
        r5 = r25.getPreviousSpanX();
        r2 = r2 / r5;
        r2 = java.lang.Math.abs(r2);
        r5 = 1;
        r0 = r24;
        r0.mInZoomTransformGestureX = r5;
    L_0x00d4:
        r5 = r25.getPreviousSpanY();
        r8 = 0;
        r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1));
        if (r5 == 0) goto L_0x0132;
    L_0x00dd:
        r5 = r25.getCurrentSpanY();
        r8 = 0;
        r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1));
        if (r5 == 0) goto L_0x0132;
    L_0x00e6:
        r0 = r24;
        r5 = r0.mInZoomTransformGestureY;
        if (r5 != 0) goto L_0x0100;
    L_0x00ec:
        r16 = 4627842681983205376; // 0x4039666660000000 float:3.6893488E19 double:25.399999618530273;
        r14 = r14 * r16;
        r5 = screenPPI;
        r0 = (double) r5;
        r16 = r0;
        r14 = r14 / r16;
        r16 = 4613937818241073152; // 0x4008000000000000 float:0.0 double:3.0;
        r5 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1));
        if (r5 <= 0) goto L_0x0132;
    L_0x0100:
        r5 = r25.getCurrentSpanY();
        r8 = r25.getPreviousSpanY();
        r5 = r5 / r8;
        r9 = java.lang.Math.abs(r5);
        r5 = 1;
        r0 = r24;
        r0.mInZoomTransformGestureY = r5;
        r8 = r2;
    L_0x0113:
        r0 = r24;
        r0.mPreviousZoomLocX = r6;
        r0 = r24;
        r0.mPreviousZoomLocY = r7;
        r0 = r24;
        r2 = r0.mCompanionView;
        r5 = 1;
        r2.onGestureListener(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12);
        r2 = 1;
        goto L_0x0013;
    L_0x0126:
        r9 = r25.getScaleFactor();
        r8 = r9;
        goto L_0x0113;
    L_0x012c:
        r2 = 0;
        goto L_0x0013;
    L_0x012f:
        r2 = 0;
        goto L_0x0013;
    L_0x0132:
        r8 = r2;
        goto L_0x0113;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.air.wand.view.GestureListener.onScale(android.view.ScaleGestureDetector):boolean");
    }

    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
        int multitouchMode = this.mCompanionView.getMultitouchMode();
        this.mCompanionView.getClass();
        if (multitouchMode == kGestureRotate && this.mInZoomTransformGesture) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            this.mCompanionView.onGestureListener(kGestureSwipe, 0, true, this.mPreviousZoomLocX, this.mPreviousZoomLocY, scaleFactor, scaleFactor, 0.0f, 0.0f, 0.0f);
        }
    }

    private float getRotation(TouchPoint touchPoint, TouchPoint touchPoint2, TouchPoint touchPoint3, TouchPoint touchPoint4) {
        if (touchPoint.pid != touchPoint3.pid || touchPoint2.pid != touchPoint4.pid) {
            return 0.0f;
        }
        return (float) (((Math.atan2((double) (touchPoint4.f10y - touchPoint3.f10y), (double) (touchPoint4.f9x - touchPoint3.f9x)) * 180.0d) / 3.141592653589793d) - ((Math.atan2((double) (touchPoint2.f10y - touchPoint.f10y), (double) (touchPoint2.f9x - touchPoint.f9x)) * 180.0d) / 3.141592653589793d));
    }

    private boolean isPanGesture(TouchPoint touchPoint, TouchPoint touchPoint2, TouchPoint touchPoint3, TouchPoint touchPoint4) {
        float access$200 = touchPoint3.f9x - touchPoint.f9x;
        float access$300 = touchPoint3.f10y - touchPoint.f10y;
        float access$2002 = touchPoint4.f9x - touchPoint2.f9x;
        float access$3002 = touchPoint4.f10y - touchPoint2.f10y;
        float min = Math.min(Math.abs(access$200), Math.abs(access$2002));
        float min2 = Math.min(Math.abs(access$300), Math.abs(access$3002));
        double sqrt = Math.sqrt((double) ((min * min) + (min2 * min2)));
        if (((access$200 < 0.0f || access$2002 < 0.0f) && (access$200 > 0.0f || access$2002 > 0.0f)) || (((access$300 < 0.0f || access$3002 < 0.0f) && (access$300 > 0.0f || access$3002 > 0.0f)) || (!this.mInPanTransformGesture && sqrt <= ((double) ((_FP_GESTURE_ZOOM_PER_AXIS_THRESHOLD_MM * ((float) screenPPI)) / MM_PER_INCH))))) {
            return false;
        }
        return true;
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        return false;
    }
}
