package org.edupage.DeviceInfoUtil;

import android.provider.Settings.Secure;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class GetCurrentDeviceIdFunction implements FREFunction {
    public FREObject call(FREContext context, FREObject[] args) {
        try {
            return FREObject.newObject(Secure.getString(context.getActivity().getApplicationContext().getContentResolver(), "android_id"));
        } catch (FREWrongThreadException e) {
            return null;
        }
    }
}
