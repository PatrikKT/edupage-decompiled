package com.adobe.fre;

import java.nio.ByteBuffer;

public class FREByteArray extends FREObject {
    private long m_dataPointer;

    public native void acquire() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native ByteBuffer getBytes() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native long getLength() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native void release() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    private FREByteArray(CFREObjectWrapper obj) {
        super(obj);
    }

    protected FREByteArray() throws FRETypeMismatchException, FREInvalidObjectException, FREASErrorException, FRENoSuchNameException, FREWrongThreadException, IllegalStateException {
        super("flash.utils.ByteArray", null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.adobe.fre.FREByteArray newByteArray() throws com.adobe.fre.FREASErrorException, com.adobe.fre.FREWrongThreadException, java.lang.IllegalStateException {
        /*
        r0 = new com.adobe.fre.FREByteArray;	 Catch:{ FRETypeMismatchException -> 0x000b, FREInvalidObjectException -> 0x0009, FRENoSuchNameException -> 0x0006 }
        r0.<init>();	 Catch:{ FRETypeMismatchException -> 0x000b, FREInvalidObjectException -> 0x0009, FRENoSuchNameException -> 0x0006 }
    L_0x0005:
        return r0;
    L_0x0006:
        r0 = move-exception;
    L_0x0007:
        r0 = 0;
        goto L_0x0005;
    L_0x0009:
        r0 = move-exception;
        goto L_0x0007;
    L_0x000b:
        r0 = move-exception;
        goto L_0x0007;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.fre.FREByteArray.newByteArray():com.adobe.fre.FREByteArray");
    }
}
