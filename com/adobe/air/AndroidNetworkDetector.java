package com.adobe.air;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;

public class AndroidNetworkDetector {
    private static final String NET_DETECT_TAG = "AndroidNetworkDetector";
    private BroadcastReceiver mReceiver;
    private long objReference;
    private boolean registered;

    /* renamed from: com.adobe.air.AndroidNetworkDetector.1 */
    class C00321 extends BroadcastReceiver {
        C00321() {
        }

        public void onReceive(Context context, Intent intent) {
            if (!isInitialStickyBroadcast()) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                if (networkInfo != null) {
                    networkInfo.getDetailedState();
                }
                AndroidNetworkDetector.this.callOnNetworkChange(AndroidNetworkDetector.this.objReference);
            }
        }
    }

    private native void callOnNetworkChange(long j);

    public AndroidNetworkDetector() {
        this.registered = false;
    }

    public void RegisterForNetworkChange(Context context, long j) {
        if (!this.registered) {
            try {
                this.mReceiver = new C00321();
                this.objReference = j;
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                context.registerReceiver(this.mReceiver, intentFilter);
                this.registered = true;
            } catch (Exception e) {
            }
        }
    }
}
