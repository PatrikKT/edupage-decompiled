package com.freshplanet.nativeExtensions;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adobe.fre.FREContext;
import com.distriqt.extension.util.Resources;

public class C2DMBroadcastReceiver extends BroadcastReceiver {
    private static final String COLOR_SEARCH_RECURSE_TIP = "SOME_SAMPLE_TEXT";
    private static int NotifId;
    private static String TAG;
    private static int customLayout;
    private static int customLayoutDescription;
    private static int customLayoutImage;
    private static int customLayoutImageContainer;
    private static int customLayoutTitle;
    private static C2DMBroadcastReceiver instance;
    private static int notificationIcon;
    private static float notification_description_size_factor;
    private static Integer notification_text_color;
    private static float notification_text_size;
    private static float notification_title_size_factor;

    static {
        TAG = "c2dmBdcastRcvr";
        NotifId = 1;
        notification_text_color = null;
        notification_title_size_factor = 1.0f;
        notification_description_size_factor = 0.8f;
    }

    public C2DMBroadcastReceiver() {
        Log.d(TAG, "Broadcast receiver started!!!!!");
    }

    public static C2DMBroadcastReceiver getInstance() {
        return instance != null ? instance : new C2DMBroadcastReceiver();
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            handleRegistration(context, intent);
        } else if (!intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
        } else {
            if (C2DMExtension.isInForeground) {
                ((NotificationManager) context.getSystemService("notification")).cancelAll();
                C2DMExtension.context.dispatchStatusEventAsync("COMING_FROM_NOTIFICATION", "");
                return;
            }
            handleMessage(context, intent);
        }
    }

    private void handleRegistration(Context context, Intent intent) {
        FREContext freContext = C2DMExtension.context;
        String registration = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            String error = intent.getStringExtra("error");
            Log.d(TAG, "Registration failed with error: " + error);
            if (freContext != null) {
                freContext.dispatchStatusEventAsync("TOKEN_FAIL", error);
            }
        } else if (intent.getStringExtra("unregistered") != null) {
            Log.d(TAG, "Unregistered successfully");
            if (freContext != null) {
                freContext.dispatchStatusEventAsync("UNREGISTERED", "unregistered");
            }
        } else if (registration != null) {
            Log.d(TAG, "Registered successfully");
            if (freContext != null) {
                freContext.dispatchStatusEventAsync("TOKEN_SUCCESS", registration);
            }
        }
    }

    public static void registerResources(Context context) {
        notificationIcon = Resources.getResourseIdByName(context.getPackageName(), "drawable", "icon_status");
        customLayout = Resources.getResourseIdByName(context.getPackageName(), "layout", "notification");
        customLayoutTitle = Resources.getResourseIdByName(context.getPackageName(), "id", "title");
        customLayoutDescription = Resources.getResourseIdByName(context.getPackageName(), "id", "text");
        customLayoutImageContainer = Resources.getResourseIdByName(context.getPackageName(), "id", "image");
        customLayoutImage = Resources.getResourseIdByName(context.getPackageName(), "drawable", "app_icon");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleMessage(android.content.Context r57, android.content.Intent r58) {
        /*
        r56 = this;
        r39 = new android.content.Intent;
        r5 = "DAILYPLAN_UPDATE";
        r0 = r39;
        r0.<init>(r5);
        r0 = r57;
        r1 = r39;
        r0.sendBroadcast(r1);
        com.freshplanet.nativeExtensions.C2DMExtension.loadData(r57);	 Catch:{ Exception -> 0x0222 }
        registerResources(r57);	 Catch:{ Exception -> 0x0222 }
        r56.extractColors(r57);	 Catch:{ Exception -> 0x0222 }
        r15 = com.freshplanet.nativeExtensions.C2DMExtension.context;	 Catch:{ Exception -> 0x0222 }
        r5 = "notification";
        r0 = r57;
        r7 = r0.getSystemService(r5);	 Catch:{ Exception -> 0x0222 }
        r7 = (android.app.NotificationManager) r7;	 Catch:{ Exception -> 0x0222 }
        r19 = notificationIcon;	 Catch:{ Exception -> 0x0222 }
        r52 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x0222 }
        r5 = "parameters";
        r0 = r58;
        r30 = r0.getStringExtra(r5);	 Catch:{ Exception -> 0x0222 }
        r17 = 0;
        r29 = 0;
        if (r30 == 0) goto L_0x0049;
    L_0x0039:
        r5 = new org.json.JSONTokener;	 Catch:{ Exception -> 0x0216 }
        r0 = r30;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0216 }
        r5 = r5.nextValue();	 Catch:{ Exception -> 0x0216 }
        r0 = r5;
        r0 = (org.json.JSONObject) r0;	 Catch:{ Exception -> 0x0216 }
        r29 = r0;
    L_0x0049:
        if (r29 == 0) goto L_0x005d;
    L_0x004b:
        r5 = "facebookId";
        r0 = r29;
        r5 = r0.has(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x005d;
    L_0x0055:
        r5 = "facebookId";
        r0 = r29;
        r17 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
    L_0x005d:
        r31 = "News count";
        if (r29 == 0) goto L_0x0073;
    L_0x0061:
        r5 = "LS1";
        r0 = r29;
        r5 = r0.has(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x0073;
    L_0x006b:
        r5 = "LS1";
        r0 = r29;
        r31 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
    L_0x0073:
        r48 = new java.util.HashMap;	 Catch:{ Exception -> 0x0222 }
        r48.<init>();	 Catch:{ Exception -> 0x0222 }
        r35 = new java.io.FileInputStream;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r5 = new java.io.File;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r51 = r57.getCacheDir();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r54 = "notification_users";
        r0 = r51;
        r1 = r54;
        r5.<init>(r0, r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r35;
        r0.<init>(r5);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r5 = r35.available();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r11 = new byte[r5];	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r35;
        r0.read(r11);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r35.close();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r50 = new java.lang.String;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r5 = "UTF-8";
        r0 = r50;
        r0.<init>(r11, r5);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r49 = new org.json.JSONObject;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r49.<init>(r50);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r22 = r49.keys();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
    L_0x00ae:
        r5 = r22.hasNext();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        if (r5 != 0) goto L_0x022f;
    L_0x00b4:
        r26 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0222 }
        r26.<init>();	 Catch:{ Exception -> 0x0222 }
        r5 = "edupage";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0421;
    L_0x00c3:
        r5 = r48.isEmpty();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x01cf;
    L_0x00c9:
        r5 = 0;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x00e6;
    L_0x00d8:
        r5 = r48.entrySet();	 Catch:{ Exception -> 0x0222 }
        r23 = r5.iterator();	 Catch:{ Exception -> 0x0222 }
    L_0x00e0:
        r5 = r23.hasNext();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0276;
    L_0x00e6:
        r5 = r20.booleanValue();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0104;
    L_0x00ec:
        r5 = "edupage_meno";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0104;
    L_0x00f6:
        r5 = r48.entrySet();	 Catch:{ Exception -> 0x0222 }
        r23 = r5.iterator();	 Catch:{ Exception -> 0x0222 }
    L_0x00fe:
        r5 = r23.hasNext();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x02d1;
    L_0x0104:
        r5 = r20.booleanValue();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x013f;
    L_0x010a:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x013f;
    L_0x0114:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = ";";
        r0 = r51;
        r5 = r5.contains(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x013f;
    L_0x0126:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = ";";
        r0 = r51;
        r45 = r5.split(r0);	 Catch:{ Exception -> 0x0222 }
        r46 = 0;
    L_0x0138:
        r0 = r45;
        r5 = r0.length;	 Catch:{ Exception -> 0x0222 }
        r0 = r46;
        if (r0 < r5) goto L_0x0339;
    L_0x013f:
        r5 = r20.booleanValue();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0183;
    L_0x0145:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0183;
    L_0x014f:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = "*";
        r0 = r51;
        r5 = r5.contains(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x0183;
    L_0x0161:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = "*";
        r0 = r51;
        r5 = r5.split(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = 0;
        r47 = r5[r51];	 Catch:{ Exception -> 0x0222 }
        r5 = r48.entrySet();	 Catch:{ Exception -> 0x0222 }
        r23 = r5.iterator();	 Catch:{ Exception -> 0x0222 }
    L_0x017d:
        r5 = r23.hasNext();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0395;
    L_0x0183:
        r5 = r20.booleanValue();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x01cf;
    L_0x0189:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x01a5;
    L_0x0193:
        r5 = "edupage_userid";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = "";
        r0 = r51;
        r5 = r5.equals(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x01cf;
    L_0x01a5:
        r5 = "edupage_meno";
        r0 = r29;
        r5 = r0.isNull(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x01c1;
    L_0x01af:
        r5 = "edupage_meno";
        r0 = r29;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = "";
        r0 = r51;
        r5 = r5.equals(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x01cf;
    L_0x01c1:
        r5 = r48.entrySet();	 Catch:{ Exception -> 0x0222 }
        r23 = r5.iterator();	 Catch:{ Exception -> 0x0222 }
    L_0x01c9:
        r5 = r23.hasNext();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x03de;
    L_0x01cf:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.m_nPocetSprav;	 Catch:{ Exception -> 0x0222 }
        r5 = r5 + 1;
        com.freshplanet.nativeExtensions.C2DMExtension.m_nPocetSprav = r5;	 Catch:{ Exception -> 0x0222 }
        r6 = NotifId;	 Catch:{ Exception -> 0x0222 }
        r28 = 1;
        r5 = "tickerText";
        r0 = r58;
        r38 = r0.getStringExtra(r5);	 Catch:{ Exception -> 0x0222 }
        r5 = "contentTitle";
        r0 = r58;
        r14 = r0.getStringExtra(r5);	 Catch:{ Exception -> 0x0222 }
        r5 = "contentText";
        r0 = r58;
        r13 = r0.getStringExtra(r5);	 Catch:{ Exception -> 0x0222 }
        r32 = r14.toString();	 Catch:{ Exception -> 0x0222 }
        r36 = r13.toString();	 Catch:{ Exception -> 0x0222 }
        r33 = r36;
        r25 = 0;
    L_0x01fd:
        r5 = r26.size();	 Catch:{ Exception -> 0x0222 }
        r0 = r25;
        if (r0 < r5) goto L_0x042a;
    L_0x0205:
        com.freshplanet.nativeExtensions.C2DMExtension.saveData(r57);	 Catch:{ Exception -> 0x0222 }
        if (r15 == 0) goto L_0x0215;
    L_0x020a:
        if (r30 != 0) goto L_0x020e;
    L_0x020c:
        r30 = "";
    L_0x020e:
        r5 = "COMING_FROM_NOTIFICATION";
        r0 = r30;
        r15.dispatchStatusEventAsync(r5, r0);	 Catch:{ Exception -> 0x0222 }
    L_0x0215:
        return;
    L_0x0216:
        r16 = move-exception;
        r5 = TAG;	 Catch:{ Exception -> 0x0222 }
        r51 = "cannot parse the object";
        r0 = r51;
        android.util.Log.d(r5, r0);	 Catch:{ Exception -> 0x0222 }
        goto L_0x0049;
    L_0x0222:
        r16 = move-exception;
        r5 = TAG;
        r51 = "Error activating application:";
        r0 = r51;
        r1 = r16;
        android.util.Log.e(r5, r0, r1);
        goto L_0x0215;
    L_0x022f:
        r44 = r22.next();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r44 = (java.lang.String) r44;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r41 = new java.util.HashMap;	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r41.<init>();	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r49;
        r1 = r44;
        r43 = r0.getJSONObject(r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r5 = "loggedUserMeno";
        r51 = "loggedUserMeno";
        r0 = r43;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r41;
        r1 = r51;
        r0.put(r5, r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r5 = "loggedUserPriezvisko";
        r51 = "loggedUserPriezvisko";
        r0 = r43;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r41;
        r1 = r51;
        r0.put(r5, r1);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        r0 = r48;
        r1 = r44;
        r2 = r41;
        r0.put(r1, r2);	 Catch:{ IOException -> 0x0273, JSONException -> 0x072e }
        goto L_0x00ae;
    L_0x0273:
        r5 = move-exception;
        goto L_0x00b4;
    L_0x0276:
        r42 = r23.next();	 Catch:{ Exception -> 0x0222 }
        r42 = (java.util.Map.Entry) r42;	 Catch:{ Exception -> 0x0222 }
        r24 = r42.getKey();	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage_userid";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.startsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x00e0;
    L_0x02c3:
        r0 = r26;
        r1 = r24;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x00e6;
    L_0x02d1:
        r42 = r23.next();	 Catch:{ Exception -> 0x0222 }
        r42 = (java.util.Map.Entry) r42;	 Catch:{ Exception -> 0x0222 }
        r24 = r42.getKey();	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.startsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x00fe;
    L_0x0306:
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage_meno";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.endsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x00fe;
    L_0x032b:
        r0 = r26;
        r1 = r24;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x0104;
    L_0x0339:
        r5 = r48.entrySet();	 Catch:{ Exception -> 0x0222 }
        r23 = r5.iterator();	 Catch:{ Exception -> 0x0222 }
    L_0x0341:
        r5 = r23.hasNext();	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x034b;
    L_0x0347:
        r46 = r46 + 1;
        goto L_0x0138;
    L_0x034b:
        r42 = r23.next();	 Catch:{ Exception -> 0x0222 }
        r42 = (java.util.Map.Entry) r42;	 Catch:{ Exception -> 0x0222 }
        r24 = r42.getKey();	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = r45[r46];	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.startsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x0341;
    L_0x0388:
        r0 = r26;
        r1 = r24;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x0341;
    L_0x0395:
        r42 = r23.next();	 Catch:{ Exception -> 0x0222 }
        r42 = (java.util.Map.Entry) r42;	 Catch:{ Exception -> 0x0222 }
        r24 = r42.getKey();	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r47;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.startsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x017d;
    L_0x03d0:
        r0 = r26;
        r1 = r24;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x017d;
    L_0x03de:
        r42 = r23.next();	 Catch:{ Exception -> 0x0222 }
        r42 = (java.util.Map.Entry) r42;	 Catch:{ Exception -> 0x0222 }
        r24 = r42.getKey();	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "edupage";
        r0 = r29;
        r1 = r51;
        r51 = r0.getString(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "#";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r0.startsWith(r5);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x01c9;
    L_0x0413:
        r0 = r26;
        r1 = r24;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r20 = java.lang.Boolean.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x01c9;
    L_0x0421:
        r5 = "";
        r0 = r26;
        r0.add(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x01cf;
    L_0x042a:
        r0 = r26;
        r1 = r25;
        r24 = r0.get(r1);	 Catch:{ Exception -> 0x0222 }
        r24 = (java.lang.String) r24;	 Catch:{ Exception -> 0x0222 }
        r5 = "";
        r0 = r24;
        if (r0 != r5) goto L_0x0469;
    L_0x043a:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.m_nPocetSprav;	 Catch:{ Exception -> 0x0222 }
        r51 = 1;
        r0 = r51;
        if (r5 <= r0) goto L_0x0465;
    L_0x0442:
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r31);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = ": ";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = com.freshplanet.nativeExtensions.C2DMExtension.m_nPocetSprav;	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.Integer.toString(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r32 = r5.toString();	 Catch:{ Exception -> 0x0222 }
    L_0x0465:
        r25 = r25 + 1;
        goto L_0x01fd;
    L_0x0469:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationId;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.containsKey(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x048b;
    L_0x0473:
        r54 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x0222 }
        r0 = r54;
        r5 = (int) r0;	 Catch:{ Exception -> 0x0222 }
        com.freshplanet.nativeExtensions.C2DMExtension.lastNotificationId = r5;	 Catch:{ Exception -> 0x0222 }
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationId;	 Catch:{ Exception -> 0x0222 }
        r51 = com.freshplanet.nativeExtensions.C2DMExtension.lastNotificationId;	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.Integer.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r1 = r51;
        r5.put(r0, r1);	 Catch:{ Exception -> 0x0222 }
    L_0x048b:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationId;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.lang.Integer) r5;	 Catch:{ Exception -> 0x0222 }
        r6 = r5.intValue();	 Catch:{ Exception -> 0x0222 }
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNumNotifications;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.containsKey(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x04b2;
    L_0x04a3:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNumNotifications;	 Catch:{ Exception -> 0x0222 }
        r51 = 1;
        r51 = java.lang.Integer.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r1 = r51;
        r5.put(r0, r1);	 Catch:{ Exception -> 0x0222 }
    L_0x04b2:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationTexts;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.containsKey(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x0600;
    L_0x04bc:
        r37 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0222 }
        r37.<init>();	 Catch:{ Exception -> 0x0222 }
        r0 = r37;
        r1 = r36;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationTexts;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r1 = r37;
        r5.put(r0, r1);	 Catch:{ Exception -> 0x0222 }
    L_0x04d1:
        r21 = 0;
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNumNotifications;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.lang.Integer) r5;	 Catch:{ Exception -> 0x0222 }
        r28 = r5.intValue();	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r0 = r28;
        if (r0 <= r5) goto L_0x0529;
    L_0x04e6:
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r31);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = ": ";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.Integer.toString(r28);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r36 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r33 = r36;
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationTexts;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.containsKey(r0);	 Catch:{ Exception -> 0x0222 }
        if (r5 == 0) goto L_0x0527;
    L_0x0513:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationTexts;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r37 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r37 = (java.util.ArrayList) r37;	 Catch:{ Exception -> 0x0222 }
        r18 = 0;
    L_0x051f:
        r5 = r37.size();	 Catch:{ Exception -> 0x0222 }
        r0 = r18;
        if (r0 < r5) goto L_0x063a;
    L_0x0527:
        r13 = r36;
    L_0x0529:
        r51 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51.<init>();	 Catch:{ Exception -> 0x0222 }
        r0 = r48;
        r1 = r24;
        r5 = r0.get(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.util.HashMap) r5;	 Catch:{ Exception -> 0x0222 }
        r54 = "loggedUserMeno";
        r0 = r54;
        r5 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r0.append(r5);	 Catch:{ Exception -> 0x0222 }
        r51 = " ";
        r0 = r51;
        r51 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r48;
        r1 = r24;
        r5 = r0.get(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.util.HashMap) r5;	 Catch:{ Exception -> 0x0222 }
        r54 = "loggedUserPriezvisko";
        r0 = r54;
        r5 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r0.append(r5);	 Catch:{ Exception -> 0x0222 }
        r32 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r14 = r32;
        r27 = new android.content.Intent;	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = r57.getPackageName();	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r51);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = ".AppEntry";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r5 = java.lang.Class.forName(r5);	 Catch:{ Exception -> 0x0222 }
        r0 = r27;
        r1 = r57;
        r0.<init>(r1, r5);	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "http://";
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r30;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r5 = android.net.Uri.parse(r5);	 Catch:{ Exception -> 0x0222 }
        r0 = r27;
        r0.setData(r5);	 Catch:{ Exception -> 0x0222 }
        r5 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r0 = r57;
        r1 = r27;
        r12 = android.app.PendingIntent.getActivity(r0, r6, r1, r5);	 Catch:{ Exception -> 0x0222 }
        r5 = android.os.Build.VERSION.SDK_INT;	 Catch:{ Exception -> 0x0222 }
        r51 = 16;
        r0 = r51;
        if (r5 < r0) goto L_0x0667;
    L_0x05c2:
        r5 = new android.support.v4.app.NotificationCompat$Builder;	 Catch:{ Exception -> 0x0222 }
        r0 = r57;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = notificationIcon;	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r5.setSmallIcon(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.setContentTitle(r14);	 Catch:{ Exception -> 0x0222 }
        r0 = r33;
        r5 = r5.setContentText(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = r5.setTicker(r13);	 Catch:{ Exception -> 0x0222 }
        r10 = r5.setContentIntent(r12);	 Catch:{ Exception -> 0x0222 }
        if (r21 == 0) goto L_0x05f1;
    L_0x05e5:
        r5 = new android.support.v4.app.NotificationCompat$BigTextStyle;	 Catch:{ Exception -> 0x0222 }
        r5.<init>();	 Catch:{ Exception -> 0x0222 }
        r5 = r5.bigText(r13);	 Catch:{ Exception -> 0x0222 }
        r10.setStyle(r5);	 Catch:{ Exception -> 0x0222 }
    L_0x05f1:
        r8 = r10.build();	 Catch:{ Exception -> 0x0222 }
        r7.notify(r6, r8);	 Catch:{ Exception -> 0x0222 }
    L_0x05f8:
        r5 = NotifId;	 Catch:{ Exception -> 0x0222 }
        r5 = r5 + 1;
        NotifId = r5;	 Catch:{ Exception -> 0x0222 }
        goto L_0x0465;
    L_0x0600:
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNotificationTexts;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r37 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r37 = (java.util.ArrayList) r37;	 Catch:{ Exception -> 0x0222 }
        r0 = r37;
        r1 = r36;
        r5 = r0.contains(r1);	 Catch:{ Exception -> 0x0222 }
        if (r5 != 0) goto L_0x04d1;
    L_0x0614:
        r0 = r37;
        r1 = r36;
        r0.add(r1);	 Catch:{ Exception -> 0x0222 }
        r51 = com.freshplanet.nativeExtensions.C2DMExtension.mapNumNotifications;	 Catch:{ Exception -> 0x0222 }
        r5 = com.freshplanet.nativeExtensions.C2DMExtension.mapNumNotifications;	 Catch:{ Exception -> 0x0222 }
        r0 = r24;
        r5 = r5.get(r0);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.lang.Integer) r5;	 Catch:{ Exception -> 0x0222 }
        r5 = r5.intValue();	 Catch:{ Exception -> 0x0222 }
        r5 = r5 + 1;
        r5 = java.lang.Integer.valueOf(r5);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r1 = r24;
        r0.put(r1, r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x04d1;
    L_0x063a:
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = java.lang.String.valueOf(r36);	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "\n";
        r0 = r51;
        r51 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r37;
        r1 = r18;
        r5 = r0.get(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = (java.lang.String) r5;	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r5 = r0.append(r5);	 Catch:{ Exception -> 0x0222 }
        r36 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r21 = 1;
        r18 = r18 + 1;
        goto L_0x051f;
    L_0x0667:
        r8 = new android.app.Notification;	 Catch:{ Exception -> 0x0222 }
        r0 = r19;
        r1 = r38;
        r2 = r52;
        r8.<init>(r0, r1, r2);	 Catch:{ Exception -> 0x0222 }
        r5 = r8.flags;	 Catch:{ Exception -> 0x0222 }
        r5 = r5 | 16;
        r8.flags = r5;	 Catch:{ Exception -> 0x0222 }
        r0 = r57;
        r8.setLatestEventInfo(r0, r14, r13, r12);	 Catch:{ Exception -> 0x0222 }
        r9 = new android.widget.RemoteViews;	 Catch:{ Exception -> 0x0222 }
        r5 = r57.getPackageName();	 Catch:{ Exception -> 0x0222 }
        r51 = customLayout;	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r9.<init>(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutTitle;	 Catch:{ Exception -> 0x0222 }
        r9.setTextViewText(r5, r14);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutDescription;	 Catch:{ Exception -> 0x0222 }
        r9.setTextViewText(r5, r13);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutTitle;	 Catch:{ Exception -> 0x0222 }
        r51 = notification_text_color;	 Catch:{ Exception -> 0x0222 }
        r51 = r51.intValue();	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r9.setTextColor(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutTitle;	 Catch:{ Exception -> 0x0222 }
        r51 = "setTextSize";
        r54 = notification_title_size_factor;	 Catch:{ Exception -> 0x0222 }
        r55 = notification_text_size;	 Catch:{ Exception -> 0x0222 }
        r54 = r54 * r55;
        r0 = r51;
        r1 = r54;
        r9.setFloat(r5, r0, r1);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutDescription;	 Catch:{ Exception -> 0x0222 }
        r51 = notification_text_color;	 Catch:{ Exception -> 0x0222 }
        r51 = r51.intValue();	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r9.setTextColor(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutDescription;	 Catch:{ Exception -> 0x0222 }
        r51 = "setTextSize";
        r54 = notification_description_size_factor;	 Catch:{ Exception -> 0x0222 }
        r55 = notification_text_size;	 Catch:{ Exception -> 0x0222 }
        r54 = r54 * r55;
        r0 = r51;
        r1 = r54;
        r9.setFloat(r5, r0, r1);	 Catch:{ Exception -> 0x0222 }
        if (r17 == 0) goto L_0x0715;
    L_0x06d2:
        r5 = TAG;	 Catch:{ Exception -> 0x0222 }
        r51 = "bitmap not null";
        r0 = r51;
        android.util.Log.d(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r4 = new com.freshplanet.nativeExtensions.CreateNotificationTask;	 Catch:{ Exception -> 0x0222 }
        r4.<init>();	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutImageContainer;	 Catch:{ Exception -> 0x0222 }
        r4.setParams(r5, r6, r7, r8, r9);	 Catch:{ Exception -> 0x0222 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0222 }
        r51 = "http://graph.facebook.com/";
        r0 = r51;
        r5.<init>(r0);	 Catch:{ Exception -> 0x0222 }
        r0 = r17;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r51 = "/picture?type=normal";
        r0 = r51;
        r5 = r5.append(r0);	 Catch:{ Exception -> 0x0222 }
        r34 = r5.toString();	 Catch:{ Exception -> 0x0222 }
        r40 = new java.net.URL;	 Catch:{ Exception -> 0x0222 }
        r0 = r40;
        r1 = r34;
        r0.<init>(r1);	 Catch:{ Exception -> 0x0222 }
        r5 = 1;
        r5 = new java.net.URL[r5];	 Catch:{ Exception -> 0x0222 }
        r51 = 0;
        r5[r51] = r40;	 Catch:{ Exception -> 0x0222 }
        r4.execute(r5);	 Catch:{ Exception -> 0x0222 }
        goto L_0x05f8;
    L_0x0715:
        r5 = TAG;	 Catch:{ Exception -> 0x0222 }
        r51 = "bitmap null";
        r0 = r51;
        android.util.Log.d(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r5 = customLayoutImageContainer;	 Catch:{ Exception -> 0x0222 }
        r51 = customLayoutImage;	 Catch:{ Exception -> 0x0222 }
        r0 = r51;
        r9.setImageViewResource(r5, r0);	 Catch:{ Exception -> 0x0222 }
        r8.contentView = r9;	 Catch:{ Exception -> 0x0222 }
        r7.notify(r6, r8);	 Catch:{ Exception -> 0x0222 }
        goto L_0x05f8;
    L_0x072e:
        r5 = move-exception;
        goto L_0x00b4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.freshplanet.nativeExtensions.C2DMBroadcastReceiver.handleMessage(android.content.Context, android.content.Intent):void");
    }

    private boolean recurseGroup(Context context, ViewGroup gp) {
        int count = gp.getChildCount();
        for (int i = 0; i < count; i++) {
            if (gp.getChildAt(i) instanceof TextView) {
                TextView text = (TextView) gp.getChildAt(i);
                if (COLOR_SEARCH_RECURSE_TIP.equals(text.getText().toString())) {
                    notification_text_color = Integer.valueOf(text.getTextColors().getDefaultColor());
                    notification_text_size = text.getTextSize();
                    DisplayMetrics metrics = new DisplayMetrics();
                    ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
                    notification_text_size /= metrics.scaledDensity;
                    return true;
                }
            } else if (gp.getChildAt(i) instanceof ViewGroup) {
                return recurseGroup(context, (ViewGroup) gp.getChildAt(i));
            }
        }
        return false;
    }

    private void extractColors(Context context) {
        if (notification_text_color == null) {
            try {
                Notification ntf = new Notification();
                ntf.setLatestEventInfo(context, COLOR_SEARCH_RECURSE_TIP, "Utest", null);
                LinearLayout group = new LinearLayout(context);
                recurseGroup(context, (ViewGroup) ntf.contentView.apply(context, group));
                group.removeAllViews();
            } catch (Exception e) {
                notification_text_color = Integer.valueOf(17170444);
            }
        }
    }
}
