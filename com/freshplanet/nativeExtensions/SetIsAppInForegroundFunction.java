package com.freshplanet.nativeExtensions;

import android.app.NotificationManager;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;

public class SetIsAppInForegroundFunction implements FREFunction {
    public FREObject call(FREContext arg0, FREObject[] arg1) {
        try {
            boolean isInForeground = arg1[0].getAsBool();
            if (isInForeground) {
                ((NotificationManager) C2DMExtension.context.getActivity().getApplicationContext().getSystemService("notification")).cancelAll();
                C2DMExtension.m_nPocetSprav = 0;
            }
            C2DMExtension.isInForeground = isInForeground;
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (FRETypeMismatchException e2) {
            e2.printStackTrace();
        } catch (FREInvalidObjectException e3) {
            e3.printStackTrace();
        } catch (FREWrongThreadException e4) {
            e4.printStackTrace();
        }
        return null;
    }
}
