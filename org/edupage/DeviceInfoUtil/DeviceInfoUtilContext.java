package org.edupage.DeviceInfoUtil;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import java.util.HashMap;
import java.util.Map;

public class DeviceInfoUtilContext extends FREContext {
    public Map<String, FREFunction> getFunctions() {
        Map<String, FREFunction> functions = new HashMap();
        functions.put("initialize", new InitializeFunction());
        functions.put("getCurrentDeviceName", new GetCurrentDeviceNameFunction());
        functions.put("getCurrentDeviceId", new GetCurrentDeviceIdFunction());
        return functions;
    }

    public void dispose() {
    }
}
