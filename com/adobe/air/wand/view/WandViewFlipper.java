package com.adobe.air.wand.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.appcompat.C0007R;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.adobe.air.wand.view.WandView.Listener;
import com.adobe.air.wand.view.WandView.ScreenOrientation;

public class WandViewFlipper extends ViewFlipper implements WandView {
    private static final String ACTIVE_WIFI_ASSIST_MESSAGE = "Enter this PIN in the desktop game and press 'Connect'";
    private static final String DEFAULT_VIEW_FONT_ASSET = "AdobeClean-Light.ttf";
    private static final String INACTIVE_WIFI_ASSIST_MESSAGE = "Connect this device to WiFi to get the pairing PIN";
    private static final String LOG_TAG = "WandViewFlipper";
    private static final String PIN_TITLE = "PIN : ";
    private static final String TITLE_DESCRIPTION_STRING = "Use this device as a Wireless Gamepad";
    private CompanionView mCompanionView;
    private View mCompanionViewHolder;
    private int mCurrentViewIndex;
    private View mDefaultView;
    private Listener mListener;
    private TouchSensor mTouchSensor;

    /* renamed from: com.adobe.air.wand.view.WandViewFlipper.1 */
    class C00811 implements Runnable {
        final /* synthetic */ Bitmap val$scaledImage;
        final /* synthetic */ ImageView val$skinView;

        C00811(ImageView imageView, Bitmap bitmap) {
            this.val$skinView = imageView;
            this.val$scaledImage = bitmap;
        }

        public void run() {
            this.val$skinView.setImageBitmap(this.val$scaledImage);
        }
    }

    /* renamed from: com.adobe.air.wand.view.WandViewFlipper.2 */
    class C00822 implements Runnable {
        C00822() {
        }

        public void run() {
            CharSequence access$300;
            boolean z;
            ((ImageView) WandViewFlipper.this.mCompanionViewHolder.findViewById(C0007R.id.skin)).setImageResource(C0007R.color.transparent);
            WandViewFlipper.this.mCurrentViewIndex = 0;
            String str = "";
            if (WandViewFlipper.this.mListener != null) {
                str = WandViewFlipper.this.mListener.getConnectionToken();
            }
            if (str.equals("")) {
                Object obj = str;
            } else {
                access$300 = WandViewFlipper.getTokenString(str);
            }
            ((TextView) WandViewFlipper.this.mDefaultView.findViewById(C0007R.id.token_string)).setText(access$300);
            TextView textView = (TextView) WandViewFlipper.this.mDefaultView.findViewById(C0007R.id.token_desc);
            if (WandViewFlipper.this.mListener.getConnectionToken().equals("")) {
                z = false;
            } else {
                z = true;
            }
            textView.setText(WandViewFlipper.getTokenDesc(z));
            WandViewFlipper.this.setDisplayedChild(WandViewFlipper.this.mCurrentViewIndex);
        }
    }

    /* renamed from: com.adobe.air.wand.view.WandViewFlipper.3 */
    class C00833 implements Runnable {
        C00833() {
        }

        public void run() {
            WandViewFlipper.this.setDisplayedChild(WandViewFlipper.this.mCurrentViewIndex);
            try {
                if (WandViewFlipper.this.mListener != null) {
                    WandViewFlipper.this.mListener.onLoadCompanion(((Activity) WandViewFlipper.this.getContext()).getResources().getConfiguration());
                }
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.adobe.air.wand.view.WandViewFlipper.4 */
    class C00844 implements Runnable {
        final /* synthetic */ String val$connectionToken;

        C00844(String str) {
            this.val$connectionToken = str;
        }

        public void run() {
            CharSequence access$300;
            String str = "";
            if (this.val$connectionToken.equals("")) {
                Object obj = str;
            } else {
                access$300 = WandViewFlipper.getTokenString(this.val$connectionToken);
            }
            ((TextView) WandViewFlipper.this.mDefaultView.findViewById(C0007R.id.token_string)).setText(access$300);
            ((TextView) WandViewFlipper.this.mDefaultView.findViewById(C0007R.id.token_desc)).setText(WandViewFlipper.getTokenDesc(!this.val$connectionToken.equals("")));
        }
    }

    /* renamed from: com.adobe.air.wand.view.WandViewFlipper.5 */
    static /* synthetic */ class C00855 {
        static final /* synthetic */ int[] $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation;

        static {
            $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation = new int[ScreenOrientation.values().length];
            try {
                $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation[ScreenOrientation.LANDSCAPE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation[ScreenOrientation.PORTRAIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation[ScreenOrientation.REVERSE_PORTRAIT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation[ScreenOrientation.REVERSE_LANDSCAPE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public WandViewFlipper(Context context) {
        super(context);
        this.mCurrentViewIndex = 0;
        this.mDefaultView = null;
        this.mCompanionViewHolder = null;
        this.mCompanionView = null;
        this.mTouchSensor = null;
        this.mListener = null;
        initView(context);
    }

    public WandViewFlipper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mCurrentViewIndex = 0;
        this.mDefaultView = null;
        this.mCompanionViewHolder = null;
        this.mCompanionView = null;
        this.mTouchSensor = null;
        this.mListener = null;
        initView(context);
    }

    private void initView(Context context) {
        this.mListener = null;
        try {
            setKeepScreenOn(true);
            LayoutInflater from = LayoutInflater.from(context);
            this.mDefaultView = from.inflate(C0007R.layout.wand_default, null);
            this.mCompanionViewHolder = from.inflate(C0007R.layout.wand_companion, null);
            this.mDefaultView.getBackground().setDither(true);
            TextView textView = (TextView) this.mDefaultView.findViewById(C0007R.id.title_string);
            Typeface createFromAsset = Typeface.createFromAsset(context.getAssets(), DEFAULT_VIEW_FONT_ASSET);
            textView.setTypeface(createFromAsset);
            ((TextView) this.mDefaultView.findViewById(C0007R.id.token_string)).setTypeface(createFromAsset);
            ((TextView) this.mDefaultView.findViewById(C0007R.id.token_desc)).setTypeface(createFromAsset);
            textView = (TextView) this.mDefaultView.findViewById(C0007R.id.title_desc);
            textView.setTypeface(createFromAsset);
            textView.setText(TITLE_DESCRIPTION_STRING);
            addView(this.mDefaultView, 0);
            addView(this.mCompanionViewHolder, 1);
            this.mCompanionView = (CompanionView) this.mCompanionViewHolder.findViewById(C0007R.id.companion_view);
            this.mTouchSensor = this.mCompanionView.getTouchSensor();
            this.mCurrentViewIndex = 0;
        } catch (Exception e) {
        }
    }

    public void setScreenOrientation(ScreenOrientation screenOrientation) throws Exception {
        int i;
        switch (C00855.$SwitchMap$com$adobe$air$wand$view$WandView$ScreenOrientation[screenOrientation.ordinal()]) {
            case GestureListener.kGestureUpdate /*1*/:
                i = 0;
                break;
            case GestureListener.kGestureRotate /*2*/:
                i = 1;
                break;
            case GestureListener.kGestureTap /*3*/:
                i = 9;
                break;
            case GestureListener.kGestureSwipe /*4*/:
                i = 8;
                break;
            default:
                i = -1;
                break;
        }
        Activity activity = (Activity) getContext();
        if (activity == null) {
            throw new IllegalArgumentException("Wand cannot find activity while loading companion.");
        }
        activity.setRequestedOrientation(i);
    }

    public void drawImage(Bitmap bitmap) throws Exception {
        if (this.mCurrentViewIndex == 0) {
            throw new Exception("Companion view is not yet loaded.");
        }
        ImageView imageView = (ImageView) this.mCompanionViewHolder.findViewById(C0007R.id.skin);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, imageView.getWidth(), (bitmap.getHeight() * imageView.getWidth()) / bitmap.getWidth(), true);
        if (createScaledBitmap != bitmap) {
            bitmap.recycle();
        }
        int height = imageView.getHeight();
        int height2 = createScaledBitmap.getHeight();
        if (height2 > height) {
            Bitmap createBitmap = Bitmap.createBitmap(createScaledBitmap, 0, height2 - height, imageView.getWidth(), imageView.getHeight());
            if (createBitmap != createScaledBitmap) {
                createScaledBitmap.recycle();
            }
            createScaledBitmap = createBitmap;
        }
        ((Activity) getContext()).runOnUiThread(new C00811(imageView, createScaledBitmap));
    }

    private static String getTokenString(String str) {
        return PIN_TITLE + str;
    }

    private static String getTokenDesc(boolean z) {
        if (z) {
            return ACTIVE_WIFI_ASSIST_MESSAGE;
        }
        return INACTIVE_WIFI_ASSIST_MESSAGE;
    }

    public void loadDefaultView() throws Exception {
        ((Activity) getContext()).runOnUiThread(new C00822());
    }

    public void loadCompanionView() throws Exception {
        if (this.mCurrentViewIndex != 1) {
            this.mCurrentViewIndex = 1;
            ((Activity) getContext()).runOnUiThread(new C00833());
        }
    }

    public void registerListener(Listener listener) throws Exception {
        if (this.mListener != null) {
            throw new Exception("View listener is already registered");
        } else if (listener == null) {
            throw new Exception("Invalid view listener");
        } else {
            this.mListener = listener;
        }
    }

    public void unregisterListener() {
        this.mListener = null;
    }

    public void updateConnectionToken(String str) {
        if (this.mCurrentViewIndex != 1) {
            ((Activity) getContext()).runOnUiThread(new C00844(str));
        }
    }

    public TouchSensor getTouchSensor() {
        return this.mTouchSensor;
    }
}
