package com.adobe.air;

public final class AndroidConstants {
    public static final String ADOBE = "adobe";
    public static final String ADOBE_AIR = "Adobe AIR";
    public static final String AIR = "AIR";
    public static final String ANDROID_RESOURCE_DEBUG_RAW_INFO = "raw.debuginfo";
    public static final String ANDROID_RESOURCE_RAW_INFO = "raw.debugger";
    public static final String ANDROID_RESOURCE_RGBA8888 = "raw.rgba8888";
    public static String DEBUGGER_INFO = null;
    public static int NETWORK_REQUEST_TIME_OUT = 0;
    public static final String PROFILER_RES_RAW_INFO = "raw.profileragent";

    static {
        NETWORK_REQUEST_TIME_OUT = 1000;
        DEBUGGER_INFO = "debugInfo";
    }
}
