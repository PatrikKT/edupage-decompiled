package com.freshplanet.nativeExtensions;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import com.adobe.fre.FREContext;
import com.distriqt.extension.util.Resources;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LocalNotificationService extends Service {
    private static final String COLOR_SEARCH_RECURSE_TIP = "SOME_SAMPLE_TEXT";
    private static int NotifId;
    private static String TAG;
    private static int customLayout;
    private static int customLayoutDescription;
    private static int customLayoutImage;
    private static int customLayoutImageContainer;
    private static int customLayoutTitle;
    private static int notificationIcon;
    private static Integer notification_text_color;
    private static float notification_text_size;

    public void onCreate() {
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (C2DMExtension.context != null) {
            Log.d("LocalNService", "context not null");
        } else {
            Log.d("LocalNService", "context is null");
        }
        handleMessage(this, intent);
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
    }

    static {
        TAG = "c2dmBdcastRcvrLcl";
        NotifId = 1;
        notification_text_color = null;
        notification_text_size = 11.0f;
    }

    private void registerResources(Context context) {
        notificationIcon = Resources.getResourseIdByName(context.getPackageName(), "drawable", "icon_status");
        customLayout = Resources.getResourseIdByName(context.getPackageName(), "layout", "notification");
        customLayoutTitle = Resources.getResourseIdByName(context.getPackageName(), "id", "title");
        customLayoutDescription = Resources.getResourseIdByName(context.getPackageName(), "id", "text");
        customLayoutImageContainer = Resources.getResourseIdByName(context.getPackageName(), "id", "image");
        customLayoutImage = Resources.getResourseIdByName(context.getPackageName(), "drawable", "app_icon");
    }

    public void handleMessage(Context context, Intent intent) {
        try {
            Log.d("LocalNService", "registering resources");
            registerResources(context);
            Log.d("LocalNService", "extract colors");
            extractColors(context);
            FREContext ctxt = C2DMExtension.context;
            NotificationManager nm = (NotificationManager) context.getSystemService("notification");
            Log.d("LocalNService", "getting nm");
            int icon = notificationIcon;
            long when = System.currentTimeMillis();
            Log.d("LocalNService", "getting extra params");
            String parameters = intent.getStringExtra("parameters");
            String facebookId = null;
            JSONObject object = null;
            if (parameters != null) {
                try {
                    object = (JSONObject) new JSONTokener(parameters).nextValue();
                } catch (Exception e) {
                    Log.d(TAG, "cannot parse the object");
                }
            }
            if (object != null) {
                if (object.has("facebookId")) {
                    facebookId = object.getString("facebookId");
                }
            }
            CharSequence tickerText = intent.getStringExtra("tickerText");
            CharSequence contentTitle = intent.getStringExtra("contentTitle");
            CharSequence contentText = intent.getStringExtra("contentText");
            Log.d("LocalNService", "creating intent");
            Intent intent2 = new Intent(context, Class.forName(context.getPackageName() + ".AppEntry"));
            Log.d("LocalNService", "getting penging intent");
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent2, 0);
            Log.d("LocalNService", "getting notif");
            Notification notification = new Notification(icon, tickerText, when);
            notification.flags |= 16;
            notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
            Log.d("LocalNService", "creating remove view");
            RemoteViews contentView = new RemoteViews(context.getPackageName(), customLayout);
            if (facebookId != null) {
                Log.d(TAG, "bitmap not null");
                HttpURLConnection connection = (HttpURLConnection) new URL("http://graph.facebook.com/" + facebookId + "/picture").openConnection();
                connection.setDoInput(true);
                connection.connect();
                Bitmap myBitmap = BitmapFactory.decodeStream(connection.getInputStream());
                contentView.setImageViewBitmap(customLayoutImageContainer, myBitmap);
            } else {
                Log.d(TAG, "bitmap null");
                contentView.setImageViewResource(customLayoutImageContainer, customLayoutImage);
            }
            contentView.setTextViewText(customLayoutTitle, contentTitle);
            contentView.setTextViewText(customLayoutDescription, contentText);
            contentView.setTextColor(customLayoutTitle, notification_text_color.intValue());
            contentView.setFloat(customLayoutTitle, "setTextSize", notification_text_size);
            contentView.setTextColor(customLayoutDescription, notification_text_color.intValue());
            contentView.setFloat(customLayoutDescription, "setTextSize", notification_text_size);
            notification.contentView = contentView;
            Log.d("LocalNService", "notifying");
            nm.notify(NotifId, notification);
            NotifId++;
            if (ctxt != null) {
                if (parameters == null) {
                    parameters = "";
                }
                ctxt.dispatchStatusEventAsync("COMING_FROM_NOTIFICATION", parameters);
            }
        } catch (Exception e2) {
            Log.e(TAG, "Error activating application:", e2);
        }
    }

    private boolean recurseGroup(Context context, ViewGroup gp) {
        int count = gp.getChildCount();
        for (int i = 0; i < count; i++) {
            if (gp.getChildAt(i) instanceof TextView) {
                TextView text = (TextView) gp.getChildAt(i);
                if (COLOR_SEARCH_RECURSE_TIP.equals(text.getText().toString())) {
                    notification_text_color = Integer.valueOf(text.getTextColors().getDefaultColor());
                    notification_text_size = text.getTextSize();
                    DisplayMetrics metrics = new DisplayMetrics();
                    ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
                    notification_text_size /= metrics.scaledDensity;
                    return true;
                }
            } else if (gp.getChildAt(i) instanceof ViewGroup) {
                return recurseGroup(context, (ViewGroup) gp.getChildAt(i));
            }
        }
        return false;
    }

    private void extractColors(Context context) {
        if (notification_text_color == null) {
            try {
                Notification ntf = new Notification();
                ntf.setLatestEventInfo(context, COLOR_SEARCH_RECURSE_TIP, "Utest", null);
                LinearLayout group = new LinearLayout(context);
                recurseGroup(context, (ViewGroup) ntf.contentView.apply(context, group));
                group.removeAllViews();
            } catch (Exception e) {
                notification_text_color = Integer.valueOf(17170444);
            }
        }
    }
}
