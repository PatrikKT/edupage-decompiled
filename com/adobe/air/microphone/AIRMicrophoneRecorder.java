package com.adobe.air.microphone;

import android.media.AudioRecord;

public class AIRMicrophoneRecorder implements Runnable {
    private byte[] mMicBuffer;
    private int m_audioFormat;
    private int m_audioSource;
    private int m_bufferSize;
    private int m_channelConfiguration;
    private volatile boolean m_isPaused;
    private volatile boolean m_isRecording;
    private AudioRecord m_recorder;
    private int m_sampleRate;
    private final Object mutex;

    public AIRMicrophoneRecorder(int i, int i2, int i3, int i4, int i5) {
        this.m_audioSource = 0;
        this.m_sampleRate = 0;
        this.m_channelConfiguration = 0;
        this.m_audioFormat = 0;
        this.m_bufferSize = 0;
        this.mutex = new Object();
        this.m_audioSource = i;
        this.m_sampleRate = i2;
        this.m_channelConfiguration = i3;
        this.m_audioFormat = i4;
        this.m_bufferSize = i5;
        this.mMicBuffer = new byte[this.m_bufferSize];
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        r4 = this;
        r1 = r4.mutex;
        monitor-enter(r1);
    L_0x0003:
        r0 = r4.isRecording();	 Catch:{ all -> 0x0018 }
        if (r0 != 0) goto L_0x001b;
    L_0x0009:
        r0 = r4.mutex;	 Catch:{ InterruptedException -> 0x000f }
        r0.wait();	 Catch:{ InterruptedException -> 0x000f }
        goto L_0x0003;
    L_0x000f:
        r0 = move-exception;
        r2 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x0018 }
        r3 = "Wait interrupted";
        r2.<init>(r3, r0);	 Catch:{ all -> 0x0018 }
        throw r2;	 Catch:{ all -> 0x0018 }
    L_0x0018:
        r0 = move-exception;
        monitor-exit(r1);	 Catch:{ all -> 0x0018 }
        throw r0;
    L_0x001b:
        monitor-exit(r1);	 Catch:{ all -> 0x0018 }
        r0 = -19;
        android.os.Process.setThreadPriority(r0);
        r0 = r4.m_recorder;
        if (r0 == 0) goto L_0x002a;
    L_0x0025:
        r0 = r4.m_recorder;	 Catch:{ IllegalStateException -> 0x002b }
        r0.startRecording();	 Catch:{ IllegalStateException -> 0x002b }
    L_0x002a:
        return;
    L_0x002b:
        r0 = move-exception;
        goto L_0x002a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.air.microphone.AIRMicrophoneRecorder.run():void");
    }

    public Boolean Open() {
        int i;
        int minBufferSize = AudioRecord.getMinBufferSize(this.m_sampleRate, this.m_channelConfiguration, this.m_audioFormat);
        if (this.m_bufferSize > minBufferSize) {
            i = this.m_bufferSize;
        } else {
            i = minBufferSize * 2;
        }
        try {
            this.m_recorder = new AudioRecord(this.m_audioSource, this.m_sampleRate, this.m_channelConfiguration, this.m_audioFormat, i);
            if (this.m_recorder == null || this.m_recorder.getState() != 1) {
                return Boolean.valueOf(false);
            }
            return Boolean.valueOf(true);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return Boolean.valueOf(false);
        }
    }

    public byte[] getBuffer() {
        int read = this.m_recorder.read(this.mMicBuffer, 0, this.m_bufferSize);
        if (read != -3 && read == -2) {
        }
        return this.mMicBuffer;
    }

    public void setRecording(boolean z) {
        synchronized (this.mutex) {
            this.m_isRecording = z;
            if (this.m_isRecording) {
                this.mutex.notify();
            } else {
                if (this.m_recorder.getState() == 1) {
                    this.m_recorder.stop();
                }
                this.m_recorder.release();
            }
        }
    }

    public boolean isRecording() {
        boolean z;
        synchronized (this.mutex) {
            z = this.m_isRecording;
        }
        return z;
    }
}
