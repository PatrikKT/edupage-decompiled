package com.adobe.air;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Locale;

public class InstallOfferPingUtils {
    private static final String LOG_TAG = "InstallOfferPingUtils";

    /* renamed from: com.adobe.air.InstallOfferPingUtils.1 */
    static class C00501 extends AsyncTask<String, Integer, Integer> {
        final /* synthetic */ boolean val$exit;

        C00501(boolean z) {
            this.val$exit = z;
        }

        protected Integer doInBackground(String... urls) {
            int resCode = 0;
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(urls[0]).openConnection();
                conn.setConnectTimeout(10000);
                conn.setRequestMethod("GET");
                HttpURLConnection.setFollowRedirects(true);
                resCode = conn.getResponseCode();
                if (this.val$exit) {
                    System.exit(0);
                }
            } catch (Exception e) {
            }
            return Integer.valueOf(resCode);
        }
    }

    public static void PingAndExit(Activity activity, String baseUrl, boolean installClicked, boolean update, boolean exit) {
        try {
            String urlQueryStr = "";
            if (update) {
                urlQueryStr = urlQueryStr + "installOffer=" + (installClicked ? "ua" : "ur");
            } else {
                urlQueryStr = urlQueryStr + "installOffer=" + (installClicked ? "a" : "r");
            }
            String urlStr = baseUrl + URLEncoder.encode(((((((urlQueryStr + "&appid=" + activity.getPackageName()) + "&runtimeType=s") + "&lang=" + Locale.getDefault().getLanguage()) + "&model=" + Build.MODEL) + "&os=a") + "&osVer=" + VERSION.RELEASE) + "&arch=" + System.getProperty("os.arch"), "UTF-8");
            new C00501(exit).execute(new String[]{urlStr});
            activity.finish();
        } catch (Exception e) {
        }
    }
}
