package org.edupage.DeviceInfoUtil;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class InitializeFunction implements FREFunction {
    public FREObject call(FREContext context, FREObject[] args) {
        try {
            return FREObject.newObject(true);
        } catch (FREWrongThreadException e) {
            return null;
        }
    }
}
