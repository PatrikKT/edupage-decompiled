package com.distriqt.extension.util;

import android.util.Log;
import java.lang.reflect.Field;

public class Resources {
    public static int getResourseIdByName(String packageName, String className, String name) {
        int id = 0;
        int i = 0;
        while (i < Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses().length) {
            try {
                if (Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i].getName().split("\\$")[1].equals(className)) {
                    if (Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i] != null) {
                        id = Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i].getField(name).getInt(Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i]);
                    }
                    return id;
                }
                i++;
            } catch (Exception e) {
                return -1;
            }
        }
        return id;
    }

    public static void listResources(String packageName) {
        int i = 0;
        while (i < Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses().length) {
            try {
                Field[] fields = Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i].getFields();
                for (Field name : fields) {
                    Log.d("Resources", new StringBuilder(String.valueOf(Class.forName(new StringBuilder(String.valueOf(packageName)).append(".R").toString()).getClasses()[i].getName())).append("::").append(name.getName()).toString());
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
