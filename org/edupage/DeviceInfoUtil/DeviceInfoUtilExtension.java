package org.edupage.DeviceInfoUtil;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class DeviceInfoUtilExtension implements FREExtension {
    public static DeviceInfoUtilContext context;

    public void initialize() {
    }

    public FREContext createContext(String contextType) {
        context = new DeviceInfoUtilContext();
        return context;
    }

    public void dispose() {
    }
}
