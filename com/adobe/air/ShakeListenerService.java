package com.adobe.air;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.content.ComponentName;
import android.content.Intent;
import com.adobe.air.ShakeListener.Listener;
import com.adobe.air.wand.WandActivity;
import java.util.List;

public class ShakeListenerService extends IntentService {
    private final String AIR_WAND_CLASS_NAME;
    private ShakeListener mShakeListener;

    /* renamed from: com.adobe.air.ShakeListenerService.1 */
    class C00661 implements Listener {
        C00661() {
        }

        public void onShake() {
            List runningTasks = ((ActivityManager) ShakeListenerService.this.getApplicationContext().getSystemService("activity")).getRunningTasks(1);
            if (!runningTasks.isEmpty()) {
                ComponentName componentName = ((RunningTaskInfo) runningTasks.get(0)).topActivity;
                if (componentName.getPackageName().equals(ShakeListenerService.this.getApplicationContext().getPackageName()) && !componentName.getClassName().equalsIgnoreCase("com.adobe.air.wand.WandActivity")) {
                    Intent intent = new Intent(ShakeListenerService.this.getApplicationContext(), WandActivity.class);
                    intent.setFlags(272629760);
                    ShakeListenerService.this.startActivity(intent);
                }
            }
        }
    }

    public ShakeListenerService() {
        super("ShakeListenerService");
        this.AIR_WAND_CLASS_NAME = "com.adobe.air.wand.WandActivity";
    }

    protected void onHandleIntent(Intent intent) {
        try {
            this.mShakeListener = new ShakeListener(this);
            this.mShakeListener.registerListener(new C00661());
        } catch (Exception e) {
        }
    }
}
