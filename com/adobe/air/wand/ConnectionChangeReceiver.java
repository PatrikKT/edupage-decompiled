package com.adobe.air.wand;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.adobe.air.wand.connection.Connection;

public class ConnectionChangeReceiver extends BroadcastReceiver {
    private static Connection mWandConn;

    public static void resisterWandConnection(Connection connection) {
        mWandConn = connection;
    }

    public static void unresisterWandConnection() {
        mWandConn = null;
    }

    public void onReceive(Context context, Intent intent) {
        if (mWandConn != null) {
            mWandConn.onConnectionChanged();
        }
    }

    static {
        mWandConn = null;
    }
}
