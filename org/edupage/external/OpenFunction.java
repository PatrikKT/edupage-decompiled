package org.edupage.external;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import java.io.File;
import java.net.URLConnection;

public class OpenFunction implements FREFunction {
    public FREObject call(FREContext context, FREObject[] args) {
        try {
            String type;
            String path = args[0].getAsString();
            try {
                type = args[1].getAsString();
            } catch (Exception e) {
                type = null;
            }
            if (type == null) {
                type = URLConnection.guessContentTypeFromName(path);
                Log.d("external", "Autodetected type: " + type);
            }
            File file = new File(path);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), type);
            intent.setFlags(1073741824);
            context.getActivity().startActivity(intent);
        } catch (Exception e2) {
        }
        return null;
    }
}
