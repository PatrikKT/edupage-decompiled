package com.freshplanet.nativeExtensions;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import java.util.HashMap;

public class ResetCountersFunction implements FREFunction {
    public FREObject call(FREContext context, FREObject[] args) {
        C2DMExtension.m_nPocetSprav = 0;
        C2DMExtension.mapNotificationId = new HashMap();
        C2DMExtension.mapNumNotifications = new HashMap();
        C2DMExtension.mapNotificationTexts = new HashMap();
        C2DMExtension.lastNotificationId = 1;
        C2DMExtension.saveData(context.getActivity().getApplicationContext());
        return null;
    }
}
