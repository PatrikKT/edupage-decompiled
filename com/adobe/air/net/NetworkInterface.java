package com.adobe.air.net;

import java.util.Vector;

public class NetworkInterface {
    public boolean active;
    private Vector<InterfaceAddress> addresses;
    public String displayName;
    public String hardwareAddress;
    public int mtu;
    public String name;
    public NetworkInterface parent;
    public NetworkInterface subInterfaces;

    public NetworkInterface() {
        this.active = false;
        this.displayName = "";
        this.hardwareAddress = "";
        this.mtu = -1;
        this.name = "";
        this.parent = null;
        this.subInterfaces = null;
        this.addresses = new Vector();
    }

    public void addAddress(InterfaceAddress interfaceAddress) {
        this.addresses.add(interfaceAddress);
    }

    public int GetAddressesCount() {
        return this.addresses.size();
    }

    public InterfaceAddress GetAddress(int i) {
        return (InterfaceAddress) this.addresses.elementAt(i);
    }
}
