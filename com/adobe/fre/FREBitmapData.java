package com.adobe.fre;

import java.nio.ByteBuffer;

public class FREBitmapData extends FREObject {
    private long m_dataPointer;

    public native void acquire() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native ByteBuffer getBits() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native int getHeight() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native int getLineStride32() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native int getWidth() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native boolean hasAlpha() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native void invalidateRect(int i, int i2, int i3, int i4) throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException, IllegalArgumentException;

    public native boolean isInvertedY() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native boolean isPremultiplied() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    public native void release() throws FREInvalidObjectException, FREWrongThreadException, IllegalStateException;

    private FREBitmapData(CFREObjectWrapper obj) {
        super(obj);
    }

    protected FREBitmapData(FREObject[] constructorArgs) throws FRETypeMismatchException, FREInvalidObjectException, FREASErrorException, FRENoSuchNameException, FREWrongThreadException, IllegalStateException {
        super("flash.display.BitmapData", constructorArgs);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.adobe.fre.FREBitmapData newBitmapData(int r8, int r9, boolean r10, java.lang.Byte[] r11) throws com.adobe.fre.FREASErrorException, com.adobe.fre.FREWrongThreadException, java.lang.IllegalArgumentException {
        /*
        r7 = 4;
        r5 = r11.length;
        if (r5 == r7) goto L_0x000c;
    L_0x0004:
        r5 = new java.lang.IllegalArgumentException;
        r6 = "fillColor has wrong length";
        r5.<init>(r6);
        throw r5;
    L_0x000c:
        r0 = new com.adobe.fre.FREObject[r7];
        r5 = 0;
        r6 = new com.adobe.fre.FREObject;
        r6.<init>(r8);
        r0[r5] = r6;
        r5 = 1;
        r6 = new com.adobe.fre.FREObject;
        r6.<init>(r9);
        r0[r5] = r6;
        r5 = 2;
        r6 = new com.adobe.fre.FREObject;
        r6.<init>(r10);
        r0[r5] = r6;
        r2 = 0;
        r4 = -1;
        r3 = 0;
    L_0x0029:
        if (r3 >= r7) goto L_0x003d;
    L_0x002b:
        r5 = 3 - r3;
        r1 = r5 * 8;
        r5 = r11[r3];
        r5 = r5.byteValue();
        r5 = r5 << r1;
        r5 = r5 & r4;
        r2 = r2 | r5;
        r4 = r4 >>> 8;
        r3 = r3 + 1;
        goto L_0x0029;
    L_0x003d:
        r5 = 3;
        r6 = new com.adobe.fre.FREObject;
        r6.<init>(r2);
        r0[r5] = r6;
        r5 = new com.adobe.fre.FREBitmapData;	 Catch:{ FRETypeMismatchException -> 0x0050, FREInvalidObjectException -> 0x004e, FRENoSuchNameException -> 0x004b }
        r5.<init>(r0);	 Catch:{ FRETypeMismatchException -> 0x0050, FREInvalidObjectException -> 0x004e, FRENoSuchNameException -> 0x004b }
    L_0x004a:
        return r5;
    L_0x004b:
        r5 = move-exception;
    L_0x004c:
        r5 = 0;
        goto L_0x004a;
    L_0x004e:
        r5 = move-exception;
        goto L_0x004c;
    L_0x0050:
        r5 = move-exception;
        goto L_0x004c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.fre.FREBitmapData.newBitmapData(int, int, boolean, java.lang.Byte[]):com.adobe.fre.FREBitmapData");
    }
}
