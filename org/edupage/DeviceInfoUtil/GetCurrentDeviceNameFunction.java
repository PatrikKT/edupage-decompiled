package org.edupage.DeviceInfoUtil;

import android.os.Build;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class GetCurrentDeviceNameFunction implements FREFunction {
    public FREObject call(FREContext context, FREObject[] args) {
        try {
            return FREObject.newObject(Build.MODEL);
        } catch (FREWrongThreadException e) {
            return null;
        }
    }
}
