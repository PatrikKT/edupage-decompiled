package com.adobe.flashruntime.shared;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;

public class VideoView extends SurfaceView {
    private static final String TAG = "VideoSurfaceView";
    private boolean mAmCreated;
    private long mCPPInstance;
    private Context mContext;
    private boolean mPlanePositionSet;
    private Surface mSurface;
    private int mXmax;
    private int mXmin;
    private int mYmax;
    private int mYmin;

    /* renamed from: com.adobe.flashruntime.shared.VideoView.1 */
    class C00881 implements Callback {
        C00881() {
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            Log.v(VideoView.TAG, "surfaceChanged format=" + i + ", width=" + i2 + ", height=" + i3);
            if (!VideoView.this.useOverlay() || !VideoView.this.mPlanePositionSet) {
                return;
            }
            if (i2 != VideoView.this.mXmax - VideoView.this.mXmin || i3 != VideoView.this.mYmax - VideoView.this.mYmin) {
                VideoView.this.setPlanePosition(VideoView.this.mXmin, VideoView.this.mYmin, VideoView.this.mXmax, VideoView.this.mYmax);
            }
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            Log.v(VideoView.TAG, "surfaceCreated");
            VideoView.this.mSurface = surfaceHolder.getSurface();
            VideoView.this.mAmCreated = true;
            VideoView.this.notifyNativeReadyForVideo();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            Log.v(VideoView.TAG, "surfaceDestroyed");
            VideoView.this.mSurface.release();
            VideoView.this.mAmCreated = false;
            VideoView.this.notifyNativeReadyForVideo();
        }
    }

    /* renamed from: com.adobe.flashruntime.shared.VideoView.2 */
    class C00892 implements Runnable {
        C00892() {
        }

        public void run() {
            VideoView.this.layout(VideoView.this.mXmin, VideoView.this.mYmin, VideoView.this.mXmax, VideoView.this.mYmax);
        }
    }

    private native void nativeSetJavaViewReady(long j, boolean z);

    public VideoView(Context context) {
        super(context);
        this.mXmin = 0;
        this.mYmin = 0;
        this.mXmax = 16;
        this.mYmax = 16;
        this.mAmCreated = false;
        this.mPlanePositionSet = false;
        this.mSurface = null;
        this.mContext = context;
        setLayoutParams(new LayoutParams(-2, -2));
        if (useOverlay()) {
            getHolder().setFormat(842094169);
        }
        getHolder().addCallback(new C00881());
    }

    public void VideoPlaybackRestarted() {
    }

    public void setFPInstance(long j) {
        Log.d(TAG, "Changing FP Instance from " + this.mCPPInstance + " to " + j);
        this.mCPPInstance = j;
        notifyNativeReadyForVideo();
    }

    public long getFPInstance() {
        return this.mCPPInstance;
    }

    public void setPlanePosition(int i, int i2, int i3, int i4) {
        this.mXmin = i;
        this.mYmin = i2;
        this.mXmax = i3;
        this.mYmax = i4;
        this.mPlanePositionSet = true;
        getHandler().post(new C00892());
    }

    protected boolean useOverlay() {
        if (VERSION.SDK_INT >= 14) {
            return true;
        }
        return false;
    }

    public Surface getSurface() {
        if (this.mAmCreated && useOverlay()) {
            return this.mSurface;
        }
        return null;
    }

    public void notifyNativeReadyForVideo() {
        if (this.mCPPInstance != 0) {
            nativeSetJavaViewReady(this.mCPPInstance, this.mAmCreated);
        }
    }
}
