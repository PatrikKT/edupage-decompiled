function hashchangeHandler() {
	var newHash = window.location.hash.substring(1);
	var data = JSON.parse(newHash);
	
	if (data.method && asc[data.method]) {		
		var ret = asc[data.method].apply(data.params ? data.params : []);		
		window.location = 'https://invokedmethod/'+encodeURIComponent(JSON.stringify(ret));
	}
};

function runMethod(method) {	
	var ret = null;
	if (asc[method]) {		
		var params = [];
		for(var i=1; i<arguments.length; i++) {			
			params.push(arguments[i]);
		}
   		ret = asc[method].apply(asc, params);
	}
	//ak je to dlhsie ako 500 znakov tak to potrebujeme rozsekat
	
	var cnt = JSON.stringify(ret);		
	methodPartStart = 0;
	methodPartCnt = cnt;
	sendMethodPart();	
}
var methodPartStart = 0;
var maxMethodPart = 100;
var methodPartCnt = '';
function sendMethodPart() {
	var substr = methodPartCnt.substr(methodPartStart,maxMethodPart);
	methodPartStart += maxMethodPart;
	var url = methodPartCnt.length > methodPartStart ? 'https://invokedpart/' : 'https://invokedmethod/';
	url += btoa(substr);	
	window.location = url;
	if (methodPartCnt.length>methodPartStart) {
		setTimeout(sendMethodPart,1);
	}	
}

var locationCounter = 0;
function runFlexMethod(method, params) {
	locationCounter++;	
	var ret = {
		method: method,
		params: params,
		locationCounter: locationCounter
	}
	var str = 'https://flexmethod/'+btoa(JSON.stringify(ret));
	
	window.location = str;
}

var asc = {};
asc.submitForm = function(id) {	
	var formElem = document.getElementById(id);
	if (!formElem) return {};	
	var data = {};
	for (var i=0;i<formElem.elements.length;i++) {
		var elem = formElem.elements[i];		
		data[elem.name] = elem.value;
	}
	
	return data;
}
asc.elementContent = function(id) {	
	var formElem = document.getElementById(id);
	if (!formElem) return '';	
	var ret = {
		'content': formElem.innerHTML
	}	
	return ret;
}

asc.loadCssStr = function(cnt) {
	var elem=document.createElement("style")    
    elem.setAttribute("type", "text/css");
    elem.innerHTML = cnt;
    document.getElementsByTagName("head")[0].appendChild(elem);    
}

asc.setElementBackgroundUrl = function(id, url) {	
	var elem = document.getElementById(id);
	
	if (!elem) {		
		return;
	}
	elem.style.backgroundImage = "url('"+url+"')";
}

asc.FastButton = function(element, handler) {
  if (element.hasFastButton) return;
  this.element = element;
  this.handler = handler;

  element.addEventListener('touchstart', this, false);
  element.addEventListener('click', this, false);
  element.hasFastButton = true;
};

asc.FastButton.prototype.handleEvent = function(event) {
  switch (event.type) {
    case 'touchstart': this.onTouchStart(event); break;    
    case 'touchmove': this.onTouchMove(event); break;    
    case 'touchend': this.onClick(event); break;
    case 'click': this.onClick(event); break;
  }
};

asc.FastButton.prototype.onTouchStart = function(event) {
  event.stopPropagation();

  this.element.addEventListener('touchend', this, false);
  document.body.addEventListener('touchmove', this, false);

  this.element.classList.toggle('fastbutton-active', true);
  this.startX = event.touches[0].clientX;
  this.startY = event.touches[0].clientY;
};

asc.FastButton.prototype.onTouchMove = function(event) {
  if (Math.abs(event.touches[0].clientX - this.startX) > 10 ||
      Math.abs(event.touches[0].clientY - this.startY) > 10) {
    this.reset();
    this.element.classList.remove('fastbutton-active');
  }
};

asc.FastButton.prototype.onClick = function(event) {
  event.stopPropagation();
  this.reset();  
  var self = this;
  self.handler.call(self, event);
  self.element.classList.remove('fastbutton-active');

  if (event.type == 'touchend') {
    asc.clickbuster.preventGhostClick(this.startX, this.startY);
  }
};

asc.FastButton.prototype.reset = function() {
  this.element.removeEventListener('touchend', this, false);
  document.body.removeEventListener('touchmove', this, false);
};
asc.clickbuster = function() {
}
asc.clickbuster.preventGhostClick = function(x, y) {
  asc.clickbuster.coordinates.push(x, y);
  window.setTimeout(asc.clickbuster.pop, 2500);
};

asc.clickbuster.pop = function() {
  asc.clickbuster.coordinates.splice(0, 2);
};

asc.clickbuster.onClick = function(event) {
  for (var i = 0; i < asc.clickbuster.coordinates.length; i += 2) {
    var x = asc.clickbuster.coordinates[i];
    var y = asc.clickbuster.coordinates[i + 1];
    if (Math.abs(event.clientX - x) < 25 && Math.abs(event.clientY - y) < 25) {
      event.stopPropagation();
      event.preventDefault();
    }
  }
};

asc.initDocument = function() {
	document.addEventListener('click', asc.clickbuster.onClick, true);
	asc.clickbuster.coordinates = [];
	
	var buttons = document.getElementsByClassName('fastbutton');
	for (var i=0;i<buttons.length;i++) {
		new asc.FastButton(buttons[i], function(event) {
			
			event.preventDefault();
			event.stopPropagation();
			var action = this.element.getAttribute('data-action');
			var location = this.element.getAttribute('data-location');
			
			if (location) {				
				window.location = location;
				return;
			}
			if (action) {				
				eval(action);				
				return;
			}			
			var href = this.element.getAttribute('href');			
			if (href) {				
				window.location = href;
				return;
			}
		});
	}
	
	this.initVideos();
}

asc.initVideos = function() {
	var iframes = document.querySelectorAll('iframe');
	
	for (var i=0;i<iframes.length;i++) {
		var iframe = iframes[i];
		var ratio = iframe.offsetHeight == 0 ? 16/9 : iframe.offsetWidth / iframe.offsetHeight;
		iframe.aspectRatio = ratio;
		 
		iframe.removeAttribute('width');
		iframe.removeAttribute('height');
		
		iframe.classList.toggle('videoIframe', true);
		
		iframe.parentNode.classList.toggle('videoWrapper', true);
	}
}