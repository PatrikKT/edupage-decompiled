package com.freshplanet.nativeExtensions;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import com.adobe.air.wand.view.CompanionView;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;
import java.util.Calendar;

public class LocalNotificationFunction implements FREFunction {
    private static int RECURRENCE_DAY;
    private static int RECURRENCE_MONTH;
    private static int RECURRENCE_NONE;
    private static int RECURRENCE_WEEK;
    private static int RECURRENCE_YEAR;
    private static String TAG;

    static {
        TAG = "LocalNotificationFunction";
        RECURRENCE_NONE = 0;
        RECURRENCE_DAY = 1;
        RECURRENCE_WEEK = 2;
        RECURRENCE_MONTH = 3;
        RECURRENCE_YEAR = 4;
    }

    public FREObject call(FREContext arg0, FREObject[] arg1) {
        if (Build.MANUFACTURER.equals("Amazon")) {
            Log.d(TAG, "push notifications disabled on amazon devices, ignoring local notification");
            return null;
        }
        String message = null;
        long timestamp = 0;
        String title = null;
        int recurrenceType = RECURRENCE_NONE;
        try {
            message = arg1[0].getAsString();
            timestamp = (long) arg1[1].getAsInt();
            title = arg1[2].getAsString();
            if (arg1.length >= 4) {
                recurrenceType = arg1[3].getAsInt();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (FRETypeMismatchException e2) {
            e2.printStackTrace();
        } catch (FREInvalidObjectException e3) {
            e3.printStackTrace();
        } catch (FREWrongThreadException e4) {
            e4.printStackTrace();
        } catch (Exception e5) {
            e5.printStackTrace();
        }
        if (message == null || timestamp <= 0) {
            Log.d(TAG, "cannot set local notification, not enough params");
        } else {
            Context appContext = arg0.getActivity().getApplicationContext();
            timestamp *= 1000;
            if (Calendar.getInstance().getTimeInMillis() > timestamp) {
                Log.d(TAG, "timestamp is older than current time");
                return null;
            }
            Intent intent = new Intent(appContext, LocalBroadcastReceiver.class);
            if (title != null) {
                intent.putExtra("contentTitle", title);
            }
            intent.putExtra("contentText", message);
            PendingIntent sender = PendingIntent.getBroadcast(appContext, 192837, intent, CompanionView.kTouchMetaStateSideButton1);
            AlarmManager am = (AlarmManager) appContext.getSystemService(NotificationCompatApi21.CATEGORY_ALARM);
            am.cancel(sender);
            if (recurrenceType == RECURRENCE_DAY) {
                am.setRepeating(0, timestamp, 86400000, sender);
            } else if (recurrenceType == RECURRENCE_WEEK) {
                am.setRepeating(0, timestamp, 604800000, sender);
            } else if (recurrenceType == RECURRENCE_MONTH) {
                am.setRepeating(0, timestamp, 964130816, sender);
            } else if (recurrenceType == RECURRENCE_YEAR) {
                am.setRepeating(0, timestamp, 1708667904, sender);
            } else {
                am.set(0, timestamp, sender);
            }
            Log.d(TAG, "setting params to run at " + Long.toString(timestamp));
        }
        return null;
    }
}
