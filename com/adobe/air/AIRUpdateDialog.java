package com.adobe.air;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;
import com.adobe.air.AndroidLocale.STRING_ID;

public class AIRUpdateDialog extends Activity {
    private static String AIR_PING_URL;
    private static AIRUpdateDialog sThis;
    private final String LOG_TAG;
    private final String RUNTIME_PACKAGE_ID;
    private AndroidActivityWrapper actWrapper;
    private AlertDialog alertDialog;

    /* renamed from: com.adobe.air.AIRUpdateDialog.1 */
    class C00181 implements OnClickListener {
        C00181() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            AIRUpdateDialog.this.actWrapper.LaunchMarketPlaceForAIR(AIRUpdateDialog.this.getIntent().getStringExtra("airDownloadURL"));
            InstallOfferPingUtils.PingAndExit(AIRUpdateDialog.sThis, AIRUpdateDialog.AIR_PING_URL, true, true, false);
        }
    }

    /* renamed from: com.adobe.air.AIRUpdateDialog.2 */
    class C00192 implements OnKeyListener {
        C00192() {
        }

        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i != 4 && i != 84) {
                return false;
            }
            dialogInterface.cancel();
            InstallOfferPingUtils.PingAndExit(AIRUpdateDialog.sThis, AIRUpdateDialog.AIR_PING_URL, false, true, false);
            return true;
        }
    }

    public AIRUpdateDialog() {
        this.LOG_TAG = "AIRUpdateDialog";
        this.alertDialog = null;
        this.RUNTIME_PACKAGE_ID = "com.adobe.air";
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        sThis = this;
        this.actWrapper = AndroidActivityWrapper.CreateAndroidActivityWrapper(this, Boolean.valueOf(false));
        this.alertDialog = new Builder(this).setTitle(AndroidConstants.ADOBE_AIR).setMessage(AndroidLocale.GetLocalizedString(STRING_ID.IDA_RUNTIME_UPDATE_MESSAGE)).create();
        this.alertDialog.setButton(-1, AndroidLocale.GetLocalizedString(STRING_ID.IDA_UPDATE), new C00181());
        this.alertDialog.setOnKeyListener(new C00192());
        this.alertDialog.setCancelable(true);
        this.alertDialog.show();
    }

    public void onPause() {
        if (this.alertDialog != null) {
            this.alertDialog.cancel();
            this.alertDialog = null;
            finish();
        }
        super.onPause();
    }

    public void onStop() {
        if (this.alertDialog != null) {
            this.alertDialog.cancel();
            this.alertDialog = null;
            finish();
        }
        super.onStop();
    }

    static {
        sThis = null;
        AIR_PING_URL = "http://airdownload2.adobe.com/air?";
    }
}
