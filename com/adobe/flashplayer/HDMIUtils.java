package com.adobe.flashplayer;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

public class HDMIUtils {
    private static final String EXTDISP_PUBLIC_STATE = "com.motorola.intent.action.externaldisplaystate";
    private static final String EXTDISP_STATUS_CONNECTION = "com.motorola.intent.action.EXTDISP_STATUS_CONNECTION";
    private static final String EXTDISP_STATUS_DISPLAY = "com.motorola.intent.action.EXTDISP_STATUS_DISPLAY";
    private static final String EXTDISP_STATUS_RESOLUTION = "com.motorola.intent.action.EXTDISP_STATUS_RESOLUTION";
    private static final String EXTRA_HDCP = "hdcp";
    private static final String EXTRA_HDMI = "hdmi";
    private static final String TAG = "HDMIUtils";
    private static final int TYPE_HDMI = 1;
    private static Object lock;
    private static HDMIUtils mSingleton;
    private HdmiServiceConnection mConnection;
    private Context mContext;
    private Binder mDeathWatcher;
    private HdmiBroadcastReceiver mExtConnectionReceiver;
    private boolean mHdcpOnOff;
    private boolean mHdmiConnection;
    private boolean mHdmiOnOff;
    private boolean mHdmiStatusKnown;

    class HdmiBroadcastReceiver extends BroadcastReceiver {
        HdmiBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            boolean z = false;
            String action = intent.getAction();
            Bundle extras = intent != null ? intent.getExtras() : null;
            boolean z2;
            if (action.equals(HDMIUtils.EXTDISP_STATUS_CONNECTION)) {
                if (extras != null) {
                    z2 = extras.getBoolean(HDMIUtils.EXTRA_HDMI);
                    if (z2 != HDMIUtils.this.mHdmiConnection) {
                        HDMIUtils.this.mHdmiConnection = z2;
                        HDMIUtils.this.mHdmiStatusKnown = true;
                    }
                }
            } else if (action.equals(HDMIUtils.EXTDISP_STATUS_DISPLAY)) {
                if (extras != null) {
                    z2 = extras.getBoolean(HDMIUtils.EXTRA_HDMI);
                    if (z2 != HDMIUtils.this.mHdmiOnOff) {
                        HDMIUtils.this.mHdmiOnOff = z2;
                        HDMIUtils.this.mHdmiStatusKnown = true;
                    }
                }
            } else if (action.equals(HDMIUtils.EXTDISP_PUBLIC_STATE) && extras != null) {
                HDMIUtils.this.mHdmiOnOff = extras.getInt(HDMIUtils.EXTRA_HDMI) == HDMIUtils.TYPE_HDMI;
                HDMIUtils hDMIUtils = HDMIUtils.this;
                if (extras.getInt(HDMIUtils.EXTRA_HDCP) == HDMIUtils.TYPE_HDMI) {
                    z = true;
                }
                hDMIUtils.mHdcpOnOff = z;
                HDMIUtils.this.mHdmiStatusKnown = true;
            }
        }
    }

    class HdmiServiceConnection implements ServiceConnection {
        HdmiServiceConnection() {
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        public void onServiceDisconnected(ComponentName componentName) {
            HDMIUtils.this.mHdmiStatusKnown = false;
        }
    }

    private enum HDMIState {
        UNKNOWN(0),
        OFF(HDMIUtils.TYPE_HDMI),
        ON(2),
        HDCPON(3);
        
        public final int value;

        private HDMIState(int i) {
            this.value = i;
        }
    }

    static {
        lock = new Object();
        mSingleton = null;
    }

    private HDMIUtils(Context context) {
        this.mHdmiStatusKnown = false;
        this.mHdmiOnOff = false;
        this.mHdcpOnOff = false;
        this.mHdmiConnection = false;
        this.mContext = null;
        this.mExtConnectionReceiver = null;
        this.mDeathWatcher = null;
        this.mContext = context;
        this.mHdmiStatusKnown = false;
        this.mHdcpOnOff = false;
        this.mHdmiOnOff = false;
        this.mHdmiConnection = false;
        this.mConnection = new HdmiServiceConnection();
        this.mDeathWatcher = new Binder();
        this.mExtConnectionReceiver = new HdmiBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(EXTDISP_PUBLIC_STATE);
        intentFilter.addAction(EXTDISP_STATUS_CONNECTION);
        intentFilter.addAction(EXTDISP_STATUS_DISPLAY);
        intentFilter.addAction(EXTDISP_STATUS_RESOLUTION);
        this.mContext.registerReceiver(this.mExtConnectionReceiver, intentFilter);
    }

    public static void initHelper(Context context) {
        if (mSingleton == null) {
            mSingleton = new HDMIUtils(context);
        }
    }

    public static void closeHelper() {
        if (mSingleton != null) {
            synchronized (lock) {
                if (mSingleton.mExtConnectionReceiver != null) {
                    mSingleton.mContext.unregisterReceiver(mSingleton.mExtConnectionReceiver);
                    mSingleton.mExtConnectionReceiver = null;
                }
                if (mSingleton.mConnection != null) {
                    mSingleton.mContext.unbindService(mSingleton.mConnection);
                    mSingleton.mConnection = null;
                }
                mSingleton.mDeathWatcher = null;
            }
            mSingleton.mHdmiStatusKnown = false;
            mSingleton = null;
        }
    }

    public boolean isConnected() {
        return this.mHdmiConnection;
    }

    public boolean isHDMIOn() {
        return this.mHdmiOnOff;
    }

    public boolean isHDCPOn() {
        return this.mHdcpOnOff;
    }

    public boolean isHdmiStatusKnown() {
        return this.mHdmiStatusKnown;
    }

    public static int getHDMIState(Context context) {
        initHelper(context);
        int i = HDMIState.UNKNOWN.value;
        if (!mSingleton.isHdmiStatusKnown()) {
            return HDMIState.UNKNOWN.value;
        }
        if (mSingleton.isHDCPOn()) {
            return HDMIState.HDCPON.value;
        }
        if (mSingleton.isHDMIOn()) {
            return HDMIState.ON.value;
        }
        return HDMIState.OFF.value;
    }
}
