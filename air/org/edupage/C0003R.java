package air.org.edupage;

/* renamed from: air.org.edupage.R */
public final class C0003R {

    /* renamed from: air.org.edupage.R.attr */
    public final class attr {
    }

    /* renamed from: air.org.edupage.R.color */
    public final class color {
        public static final int opaque_blue = 2131099648;
    }

    /* renamed from: air.org.edupage.R.drawable */
    public final class drawable {
        public static final int air_72px_mobile_eula = 2130837504;
        public static final int app_icon = 2130837505;
        public static final int home = 2130837506;
        public static final int icon = 2130837507;
        public static final int icon_status = 2130837508;
        public static final int mp_warning_32x32_n = 2130837509;
        public static final int ouya_icon = 2130837510;
    }

    /* renamed from: air.org.edupage.R.id */
    public final class id {
        public static final int CertificateDetails = 2131230742;
        public static final int Footer = 2131230741;
        public static final int Header = 2131230735;
        public static final int Line1 = 2131230740;
        public static final int Line2 = 2131230743;
        public static final int ROW1 = 2131230736;
        public static final int ROW2 = 2131230738;
        public static final int ServerName = 2131230739;
        public static final int TrustQuestion = 2131230744;
        public static final int WarningImage = 2131230737;
        public static final int button_cancel = 2131230730;
        public static final int button_ok = 2131230729;
        public static final int empty = 2131230728;
        public static final int file_save_button = 2131230725;
        public static final int file_save_label = 2131230724;
        public static final int file_save_name = 2131230726;
        public static final int file_save_panel = 2131230723;
        public static final int filecheck = 2131230722;
        public static final int filename = 2131230720;
        public static final int filepath = 2131230721;
        public static final int image = 2131230732;
        public static final int layout = 2131230731;
        public static final int list = 2131230727;
        public static final int text = 2131230734;
        public static final int title = 2131230733;
    }

    /* renamed from: air.org.edupage.R.layout */
    public final class layout {
        public static final int expandable_chooser_row = 2130903040;
        public static final int expandable_multiple_chooser_row = 2130903041;
        public static final int main = 2130903042;
        public static final int multiple_file_selection_panel = 2130903043;
        public static final int notification = 2130903044;
        public static final int ssl_certificate_warning = 2130903045;
    }

    /* renamed from: air.org.edupage.R.raw */
    public final class raw {
        public static final int adobelogo = 2130968576;
        public static final int debugger = 2130968577;
        public static final int debuginfo = 2130968578;
        public static final int icon = 2130968579;
        public static final int mms_cfg = 2130968580;
        public static final int rgba8888 = 2130968581;
        public static final int ss_cfg = 2130968582;
        public static final int ss_sgn = 2130968583;
        public static final int startga = 2130968584;
    }

    /* renamed from: air.org.edupage.R.string */
    public final class string {
        public static final int IDA_APP_DEBUGGER_TIMEOUT_INFO = 2131165214;
        public static final int IDA_APP_UNABLE_LISTEN_ERROR = 2131165212;
        public static final int IDA_APP_WAITING_DEBUGGER_TITLE = 2131165211;
        public static final int IDA_APP_WAITING_DEBUGGER_WARNING = 2131165213;
        public static final int IDA_CERTIFICATE_DETAILS = 2131165210;
        public static final int IDA_CURL_INTERFACE_ALLSESS = 2131165204;
        public static final int IDA_CURL_INTERFACE_CERTIFICATE_DETAILS_TITLE = 2131165209;
        public static final int IDA_CURL_INTERFACE_CNAME_MSG = 2131165207;
        public static final int IDA_CURL_INTERFACE_NOSESS = 2131165201;
        public static final int IDA_CURL_INTERFACE_OK = 2131165202;
        public static final int IDA_CURL_INTERFACE_SERVER = 2131165205;
        public static final int IDA_CURL_INTERFACE_THISSESS = 2131165200;
        public static final int IDA_CURL_INTERFACE_TRUSTSER = 2131165206;
        public static final int IDA_CURL_INTERFACE_UNVERSER_2 = 2131165203;
        public static final int IDA_CURL_INTERFACE_VIEW_CERT = 2131165208;
        public static final int IDA_CURL_SSL_SECURITY_WARNING = 2131165199;
        public static final int app_name = 2131165198;
        public static final int app_version = 2131165215;
        public static final int application_name = 2131165184;
        public static final int audio_files = 2131165186;
        public static final int button_cancel = 2131165191;
        public static final int button_continue = 2131165192;
        public static final int button_exit = 2131165217;
        public static final int button_install = 2131165216;
        public static final int button_install_air_from_playstore = 2131165227;
        public static final int button_no = 2131165197;
        public static final int button_ok = 2131165190;
        public static final int button_yes = 2131165196;
        public static final int empty_file_list = 2131165189;
        public static final int file_download = 2131165194;
        public static final int file_save_as = 2131165193;
        public static final int file_upload = 2131165195;
        public static final int flash_browser_plugin = 2131165185;
        public static final int image_files = 2131165187;
        public static final int text_air_missing_header = 2131165225;
        public static final int text_air_missing_text = 2131165226;
        public static final int text_dont_show_again = 2131165228;
        public static final int text_gamepreview_loading = 2131165222;
        public static final int text_gamepreview_loading_error = 2131165223;
        public static final int text_install_gamepreview_app = 2131165224;
        public static final int text_install_runtime = 2131165220;
        public static final int text_runtime_on_external_storage = 2131165221;
        public static final int text_runtime_required = 2131165219;
        public static final int title_adobe_air = 2131165218;
        public static final int video_files = 2131165188;
    }

    /* renamed from: air.org.edupage.R.style */
    public final class style {
        public static final int NotificationText = 2131034112;
        public static final int NotificationTitle = 2131034113;
        public static final int Theme_NoShadow = 2131034114;
    }
}
