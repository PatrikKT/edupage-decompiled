package com.adobe.air.wand.view;

import com.adobe.air.TouchEventData;

public class TouchSensor {
    private static final String LOG_TAG = "TouchSensor";
    private boolean mGestureEventListening;
    private Listener mListener;
    private boolean mTouchEventListening;

    public interface Listener {
        void onGestureEvent(GestureEventData gestureEventData);

        void onTouchEvent(TouchEventData touchEventData);
    }

    public TouchSensor() {
        this.mListener = null;
        this.mTouchEventListening = false;
        this.mGestureEventListening = false;
    }

    public void TouchListener() {
    }

    public void setListener(Listener listener) throws Exception {
        if (listener == null) {
            throw new Exception("Invalid Listener");
        }
        this.mListener = listener;
    }

    void dispatchEvent(TouchEventData touchEventData) {
        if (this.mTouchEventListening && this.mListener != null) {
            this.mListener.onTouchEvent(touchEventData);
        }
    }

    void dispatchEvent(GestureEventData gestureEventData) {
        if (this.mGestureEventListening && this.mListener != null) {
            this.mListener.onGestureEvent(gestureEventData);
        }
    }

    public void startTouchEventListening() {
        this.mTouchEventListening = true;
    }

    public void stopTouchEventListening() {
        this.mTouchEventListening = false;
    }

    public void startGestureEventListening() {
        this.mGestureEventListening = true;
    }

    public void stopGestureEventListening() {
        this.mGestureEventListening = false;
    }

    public boolean activeTouchListening() {
        return this.mTouchEventListening;
    }

    public boolean activeGestureListening() {
        return this.mGestureEventListening;
    }
}
