package com.adobe.air.wand.motionsensor;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public abstract class MotionSensor {
    protected static final String LOG_TAG = "MotionSensor";
    protected Activity mActivity;
    protected boolean mRegistered;
    protected Sensor mSensor;
    protected int mSensorDelay;
    protected SensorEventListener mSensorEventListener;
    protected SensorManager mSensorManager;

    /* renamed from: com.adobe.air.wand.motionsensor.MotionSensor.1 */
    class C00751 implements SensorEventListener {
        final /* synthetic */ Listener val$listener;

        C00751(Listener listener) {
            this.val$listener = listener;
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            this.val$listener.onSensorChanged(MotionSensor.this.getScreenOrientedValues(sensorEvent.values), sensorEvent.timestamp);
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }

    public interface Listener {
        void onSensorChanged(float[] fArr, long j);
    }

    public MotionSensor(Activity activity, int i) {
        this.mSensorManager = null;
        this.mSensor = null;
        this.mSensorEventListener = null;
        this.mRegistered = false;
        this.mSensorDelay = 1;
        this.mActivity = null;
        this.mActivity = activity;
        this.mSensorManager = (SensorManager) this.mActivity.getSystemService("sensor");
        this.mSensor = this.mSensorManager.getDefaultSensor(i);
        if (this.mSensor == null) {
        }
    }

    private float[] getScreenOrientedValues(float[] fArr) {
        float[] fArr2 = (float[]) fArr.clone();
        int rotation = this.mActivity.getWindowManager().getDefaultDisplay().getRotation();
        if (rotation != 0) {
            if (rotation == 2) {
                fArr2[0] = -fArr2[0];
                fArr2[1] = -fArr2[1];
            } else if (rotation == 1) {
                r1 = fArr2[0];
                fArr2[0] = -fArr2[1];
                fArr2[1] = r1;
            } else if (rotation == 3) {
                r1 = -fArr2[0];
                fArr2[0] = fArr2[1];
                fArr2[1] = r1;
            }
        }
        return fArr2;
    }

    public void setListener(Listener listener) {
        SensorEventListener sensorEventListener;
        boolean z = this.mRegistered;
        stop();
        if (listener == null) {
            sensorEventListener = null;
        } else {
            sensorEventListener = new C00751(listener);
        }
        this.mSensorEventListener = sensorEventListener;
        if (z) {
            start();
        }
    }

    public void start(int i) {
        if (!this.mRegistered && this.mSensor != null) {
            this.mSensorDelay = i * 1000;
            start();
        }
    }

    public void start() {
        if (!this.mRegistered && this.mSensor != null) {
            this.mRegistered = this.mSensorManager.registerListener(this.mSensorEventListener, this.mSensor, this.mSensorDelay);
            if (!this.mRegistered) {
            }
        }
    }

    public void stop() {
        if (this.mRegistered) {
            this.mSensorManager.unregisterListener(this.mSensorEventListener, this.mSensor);
            this.mRegistered = false;
        }
    }

    public boolean available() {
        return this.mSensor != null;
    }

    public boolean active() {
        return this.mRegistered;
    }

    public void dispose() {
        stop();
        setListener(null);
        this.mSensor = null;
        this.mSensorManager = null;
        this.mActivity = null;
    }
}
