package com.adobe.air.wand.message.json;

import com.adobe.air.wand.message.Message;
import com.adobe.air.wand.message.Message.Type;
import com.adobe.air.wand.message.MessageDataArray;
import com.adobe.air.wand.message.MessageDataObject;
import com.adobe.air.wand.message.MessageManager;
import com.adobe.air.wand.message.Notification;
import com.adobe.air.wand.message.Request;
import com.adobe.air.wand.message.Request.Data;
import com.adobe.air.wand.message.Request.Header;
import com.adobe.air.wand.message.Response;
import com.adobe.air.wand.message.Response.Status;
import org.json.JSONObject;

public class JSONMessageManager extends MessageManager {

    /* renamed from: com.adobe.air.wand.message.json.JSONMessageManager.1 */
    static /* synthetic */ class C00741 {
        static final /* synthetic */ int[] $SwitchMap$com$adobe$air$wand$message$Message$Type;

        static {
            $SwitchMap$com$adobe$air$wand$message$Message$Type = new int[Type.values().length];
            try {
                $SwitchMap$com$adobe$air$wand$message$Message$Type[Type.REQUEST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$adobe$air$wand$message$Message$Type[Type.RESPONSE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$adobe$air$wand$message$Message$Type[Type.NOTIFICATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public MessageDataObject createDataObject() {
        return new JSONMessageDataObject();
    }

    public MessageDataArray createDataArray() {
        return new JSONMessageDataArray();
    }

    public String serializeMessage(Message message) throws Exception {
        return createJSONMessage(message).toString();
    }

    public Message deserializeWandMessage(String str) throws Exception {
        return createWandMessage(new JSONObject(str));
    }

    public Request createWandRequest(String str, String str2, MessageDataArray messageDataArray) throws Exception {
        Header header = new Header(str, str2, System.currentTimeMillis());
        if (messageDataArray == null) {
            messageDataArray = new JSONMessageDataArray();
        }
        return new Request(header, new Data(messageDataArray));
    }

    public Response createWandResponse(String str, String str2, MessageDataObject messageDataObject, Status status) throws Exception {
        Response.Header header = new Response.Header(str, str2, System.currentTimeMillis(), status);
        if (messageDataObject == null) {
            messageDataObject = new JSONMessageDataObject();
        }
        return new Response(header, new Response.Data(messageDataObject));
    }

    public Notification createWandNotification(String str, MessageDataObject messageDataObject) throws Exception {
        Notification.Header header = new Notification.Header(str, System.currentTimeMillis());
        if (messageDataObject == null) {
            messageDataObject = new JSONMessageDataObject();
        }
        return new Notification(header, new Notification.Data(messageDataObject));
    }

    private static Message createWandMessage(JSONObject jSONObject) throws Exception {
        Message message;
        synchronized (jSONObject) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("header");
            JSONObject jSONObject3 = jSONObject.getJSONObject("data");
            String string = jSONObject2.getString("title");
            String string2 = jSONObject2.getString("type");
            long j = jSONObject2.getLong("timestamp");
            message = null;
            if (string2.equals(Type.REQUEST.toString())) {
                message = new Request(new Header(string, jSONObject2.getString("taskID"), j), new Data(new JSONMessageDataArray(jSONObject3.getJSONArray("arguments"))));
            } else if (string2.equals(Type.RESPONSE.toString())) {
                Status status;
                string2 = jSONObject2.getString("taskID");
                String string3 = jSONObject2.getString(NotificationCompatApi21.CATEGORY_STATUS);
                if (string3.equals(Status.SUCCESS.toString())) {
                    status = Status.SUCCESS;
                } else if (string3.equals(Status.ERROR.toString())) {
                    status = Status.ERROR;
                } else {
                    throw new Exception("Unable to fetch Response status");
                }
                message = new Response(new Response.Header(string, string2, j, status), new Response.Data(new JSONMessageDataObject(jSONObject3.getJSONObject("result"))));
            } else if (string2.equals(Type.NOTIFICATION.toString())) {
                message = new Notification(new Notification.Header(string, j), new Notification.Data(new JSONMessageDataObject(jSONObject3.getJSONObject("notification"))));
            }
        }
        return message;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.json.JSONObject createJSONMessage(com.adobe.air.wand.message.Message r6) throws java.lang.Exception {
        /*
        monitor-enter(r6);
        r0 = r6.getHeader();	 Catch:{ all -> 0x0026 }
        r0 = r0.getType();	 Catch:{ all -> 0x0026 }
        r2 = new org.json.JSONObject;	 Catch:{ all -> 0x0026 }
        r2.<init>();	 Catch:{ all -> 0x0026 }
        r3 = new org.json.JSONObject;	 Catch:{ all -> 0x0026 }
        r3.<init>();	 Catch:{ all -> 0x0026 }
        r1 = com.adobe.air.wand.message.json.JSONMessageManager.C00741.$SwitchMap$com$adobe$air$wand$message$Message$Type;	 Catch:{ all -> 0x0026 }
        r0 = r0.ordinal();	 Catch:{ all -> 0x0026 }
        r0 = r1[r0];	 Catch:{ all -> 0x0026 }
        switch(r0) {
            case 1: goto L_0x0029;
            case 2: goto L_0x007b;
            case 3: goto L_0x00ab;
            default: goto L_0x001e;
        };	 Catch:{ all -> 0x0026 }
    L_0x001e:
        r0 = new java.lang.Exception;	 Catch:{ all -> 0x0026 }
        r1 = "Unsupported message type";
        r0.<init>(r1);	 Catch:{ all -> 0x0026 }
        throw r0;	 Catch:{ all -> 0x0026 }
    L_0x0026:
        r0 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x0026 }
        throw r0;
    L_0x0029:
        r0 = r6.getHeader();	 Catch:{ all -> 0x0026 }
        r0 = (com.adobe.air.wand.message.Request.Header) r0;	 Catch:{ all -> 0x0026 }
        r1 = r6.getData();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.Request.Data) r1;	 Catch:{ all -> 0x0026 }
        r4 = "taskID";
        r5 = r0.getTaskID();	 Catch:{ all -> 0x0026 }
        r2.put(r4, r5);	 Catch:{ all -> 0x0026 }
        r4 = "arguments";
        r1 = r1.getArguments();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.json.JSONMessageDataArray) r1;	 Catch:{ all -> 0x0026 }
        r1 = r1.mJSONArray;	 Catch:{ all -> 0x0026 }
        r3.put(r4, r1);	 Catch:{ all -> 0x0026 }
    L_0x004b:
        r1 = "title";
        r4 = r0.getTitle();	 Catch:{ all -> 0x0026 }
        r2.put(r1, r4);	 Catch:{ all -> 0x0026 }
        r1 = "type";
        r4 = r0.getType();	 Catch:{ all -> 0x0026 }
        r4 = r4.toString();	 Catch:{ all -> 0x0026 }
        r2.put(r1, r4);	 Catch:{ all -> 0x0026 }
        r1 = "timestamp";
        r4 = r0.getTimestamp();	 Catch:{ all -> 0x0026 }
        r2.put(r1, r4);	 Catch:{ all -> 0x0026 }
        r0 = new org.json.JSONObject;	 Catch:{ all -> 0x0026 }
        r0.<init>();	 Catch:{ all -> 0x0026 }
        r1 = "header";
        r0.put(r1, r2);	 Catch:{ all -> 0x0026 }
        r1 = "data";
        r0.put(r1, r3);	 Catch:{ all -> 0x0026 }
        monitor-exit(r6);	 Catch:{ all -> 0x0026 }
        return r0;
    L_0x007b:
        r0 = r6.getHeader();	 Catch:{ all -> 0x0026 }
        r0 = (com.adobe.air.wand.message.Response.Header) r0;	 Catch:{ all -> 0x0026 }
        r1 = r6.getData();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.Response.Data) r1;	 Catch:{ all -> 0x0026 }
        r4 = "status";
        r5 = r0.getStatus();	 Catch:{ all -> 0x0026 }
        r5 = r5.toString();	 Catch:{ all -> 0x0026 }
        r2.put(r4, r5);	 Catch:{ all -> 0x0026 }
        r4 = "taskID";
        r5 = r0.getTaskID();	 Catch:{ all -> 0x0026 }
        r2.put(r4, r5);	 Catch:{ all -> 0x0026 }
        r4 = "result";
        r1 = r1.getResult();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.json.JSONMessageDataObject) r1;	 Catch:{ all -> 0x0026 }
        r1 = r1.mJSONObject;	 Catch:{ all -> 0x0026 }
        r3.put(r4, r1);	 Catch:{ all -> 0x0026 }
        goto L_0x004b;
    L_0x00ab:
        r0 = r6.getHeader();	 Catch:{ all -> 0x0026 }
        r0 = (com.adobe.air.wand.message.Notification.Header) r0;	 Catch:{ all -> 0x0026 }
        r1 = r6.getData();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.Notification.Data) r1;	 Catch:{ all -> 0x0026 }
        r4 = "notification";
        r1 = r1.getNotification();	 Catch:{ all -> 0x0026 }
        r1 = (com.adobe.air.wand.message.json.JSONMessageDataObject) r1;	 Catch:{ all -> 0x0026 }
        r1 = r1.mJSONObject;	 Catch:{ all -> 0x0026 }
        r3.put(r4, r1);	 Catch:{ all -> 0x0026 }
        goto L_0x004b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.air.wand.message.json.JSONMessageManager.createJSONMessage(com.adobe.air.wand.message.Message):org.json.JSONObject");
    }
}
